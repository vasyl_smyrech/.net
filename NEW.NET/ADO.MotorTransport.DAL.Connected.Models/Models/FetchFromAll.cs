﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO.MotorTransport.DAL.Connected.Models.ADO.MotorTransport.DAL.Connected.Models
{
    public class FetchFromAll
    {
        public string ClassCategory { get; set; }
        public string CategoryDescription { get; set; }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int IdVehicleName { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
    }
}
