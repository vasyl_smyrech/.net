﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO.MotorTransport.DAL.Connected.Models
{
    public class DriveCategory
    {
        public int Id { get; set; }
        public string ClassCategory { get; set; }
        public string CategoryDescription { get; set; }
    }
}

