﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ADO.MotorTransport.DAL.Connected.Models.Extensions
{
    public static class SqlConnectionExtension
    {
        public static SqlCommand GetNonQueryCommand(this SqlConnection connection, string query, CommandType commandType = CommandType.Text)
        {
            var command = connection.CreateCommand();
            command.CommandText = query;
            command.CommandType = CommandType.Text;
            command.Connection = connection;

            return command;
        }
    }
}
