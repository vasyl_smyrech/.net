﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace ADO.MotorTransport.DAL.Connected.Models.Repositories
{
    public class VehiclesRepo : BaseRepo, IRepo<VehiclesNames, int>
    {
        public void Add(VehiclesNames model)
        {
            this.ExecuteNonQuery(
                $@"INSERT INTO VehiclesNames
        ( {nameof(VehiclesNames.Manufacturer)}, {nameof(VehiclesNames.Model)}) 
        VALUES('{model.Manufacturer}', '{model.Model}')");
        }

        public void Delete(int id)
        {
            this.ExecuteNonQuery(
                $"DELETE FROM {nameof(VehiclesNames)} WHERE IdVehicleName = {id}"
            );
        }

        public IEnumerable<VehiclesNames> Get()
        {
            return this.ExecuteWithConnection((SqlConnection connection) =>
            {
                string strSQL = "SELECT * FROM Vehicle";
                var result = new List<VehiclesNames>();

                using (SqlCommand getItemsCommand = new SqlCommand(strSQL, connection))
                {
                    SqlDataReader reader = getItemsCommand.ExecuteReader(CommandBehavior.Default);

                    while (reader.Read())
                    {
                        result.Add(new VehiclesNames
                        {
                            IdVehicleName = int.Parse(reader[nameof(VehiclesNames.IdVehicleName)].ToString().Trim()),
                            Manufacturer = reader[nameof(VehiclesNames.Manufacturer)].ToString().Trim(),
                            Model = reader[nameof(VehiclesNames.Model)].ToString().Trim()
                        });
                    }
                    reader.Close();
                }
                return result;
            });
        }

        public void Update(VehiclesNames model)
        {
            try
            {
                this.ExecuteNonQuery(
            $@"UPDATE dbo.VehiclesNames
            SET {nameof(VehiclesNames.Manufacturer)} = '{model.Manufacturer}',
            {nameof(VehiclesNames.Model)} = '{model.Model}'
            WHERE {nameof(VehiclesNames.IdVehicleName)} = {model.IdVehicleName};
        ");
            }
            catch (Exception) { throw; }
        }
    }
}
