﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADO.MotorTransport.DAL.Connected.Models.ADO.MotorTransport.DAL.Connected.Models;

namespace ADO.MotorTransport.DAL.Connected.Models.Repositories
{
    public class FetchSet : BaseRepo
    {
        public IEnumerable<FetchFromAll> Get()
        {
            return this.ExecuteWithConnection((SqlConnection connection) =>
            {
                string strSQL = $@"SELECT vn.Manufacturer, vn.Model, d.FirstName, d.SecondName, d.IdDriver, dc.ClassCategory, dc.CategoryDescription
                  FROM VehiclesNames AS vn
                  LEFT JOIN VehicleNamesAndCategory AS vnc ON vn.IdVehicleName = vnc.IdVehicleName
	              LEFT JOIN DriveCategory AS dc ON vnc.ClassCategory = dc.ClassCategory
	              LEFT JOIN DriversAndCategories AS ddc ON dc.ClassCategory = ddc.ClassCategory
	              LEFT JOIN Drivers AS d ON ddc.IdDriver = d.IdDriver";
                var result = new List<FetchFromAll>();

                using (SqlCommand getItemsCommand = new SqlCommand(strSQL, connection))
                {
                    SqlDataReader reader = getItemsCommand.ExecuteReader(CommandBehavior.Default);

                    while (reader.Read())
                    {
                        result.Add(new FetchFromAll
                        {
                            IdDriver = int.Parse(reader[nameof(Drivers.IdDriver)].ToString().Trim()),
                            FirstName = reader[nameof(Drivers.FirstName)].ToString().Trim(),
                            SecondName = reader[nameof(Drivers.SecondName)].ToString().Trim(),
                            CategoryDescription = reader[nameof(DriveCategory.CategoryDescription)].ToString().Trim(),
                            ClassCategory = reader[nameof(DriveCategory.ClassCategory)].ToString().Trim(),
                            Manufacturer = reader[nameof(VehiclesNames.Manufacturer)].ToString().Trim(),
                            Model = reader[nameof(VehiclesNames.Model)].ToString().Trim()
                        });
                    }

                    reader.Close();
                }

                return result;
            });
        }
    }
}
