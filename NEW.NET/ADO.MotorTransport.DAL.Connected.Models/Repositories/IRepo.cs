﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO.MotorTransport.DAL.Connected.Models.Repositories
{
    public interface IRepo<TEntity, TId>
    where TEntity : class
    where TId : struct
    {
        void Add(TEntity model);
        void Delete(TId id);
        IEnumerable<TEntity> Get();
        void Update(TEntity model);
    }
}
