﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
namespace ADO.MotorTransport.DAL.Connected.Models.Repositories
{
    public class CategoryRepo : BaseRepo, IRepo<DriveCategory, int>
    {
        public void Add(DriveCategory model)
        {
            this.ExecuteNonQuery(
                $@"INSERT INTO DriveCategory
            ( {nameof(DriveCategory.ClassCategory)}, {nameof(DriveCategory.CategoryDescription)} ) 
            VALUES('{model.ClassCategory}', '{model.CategoryDescription}')");
        }

        public void Delete(int id)
        {
            this.ExecuteNonQuery(
                $"DELETE FROM {nameof(DriveCategory)} WHERE {nameof(DriveCategory.ClassCategory)} = '{id}'"       
            );
        }

        public IEnumerable<DriveCategory> Get()
        {
            return this.ExecuteWithConnection((SqlConnection connection) =>
            {
                string strSQL = "SELECT * FROM DriveCategory";
                var result = new List<DriveCategory>();

                using (SqlCommand getItemsCommand = new SqlCommand(strSQL, connection))
                {
                    SqlDataReader reader = getItemsCommand.ExecuteReader(CommandBehavior.Default);

                    while (reader.Read())
                    {
                        result.Add(new DriveCategory
                        {
                            ClassCategory = reader[nameof(DriveCategory.ClassCategory)].ToString().Trim(),
                            CategoryDescription = reader[nameof(DriveCategory.CategoryDescription)].ToString().Trim()
                        });
                    }

                    reader.Close();
                }

                return result;
            });
        }

        public void Update(DriveCategory model)
        {
            this.ExecuteNonQuery(
                $@"UPDATE {nameof(DriveCategory)} 
                SET {nameof(DriveCategory.CategoryDescription)} = '{model.CategoryDescription}'
                WHERE ClassCategory = '{model.ClassCategory}'
            ");
        }
    }
}
