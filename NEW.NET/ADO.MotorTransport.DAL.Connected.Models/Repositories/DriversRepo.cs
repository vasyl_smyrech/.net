﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace ADO.MotorTransport.DAL.Connected.Models.Repositories
{
    public class DriversRepo : BaseRepo, IRepo<Drivers, int>
    {
        public void Add(Drivers model)
        {
            this.ExecuteNonQuery(
                $@"INSERT INTO {nameof(Drivers)}
            ( {nameof(Drivers.FirstName)}, {nameof(Drivers.MiddleName)}, {nameof(Drivers.SecondName)}, {nameof(Drivers.DateOfBirth)}) 
            VALUES('{model.FirstName}', '{model.MiddleName}', '{model.SecondName}', '{model.DateOfBirth}')");
        }

        public void Delete(int id)
        {
            this.ExecuteNonQuery(
                $"DELETE FROM {nameof(Drivers)} WHERE {nameof(Drivers.IdDriver)} = {id}"
            );
        }

        public IEnumerable<Drivers> Get()
        {
            return this.ExecuteWithConnection((SqlConnection connection) =>
            {
                string strSQL = $@"SELECT * FROM {nameof(Drivers)}";
                var result = new List<Drivers>();

                using (SqlCommand getItemsCommand = new SqlCommand(strSQL, connection))
                {
                    SqlDataReader reader = getItemsCommand.ExecuteReader(CommandBehavior.Default);

                    while (reader.Read())
                    {
                        result.Add(new Drivers
                        {
                            IdDriver = int.Parse(reader[nameof(Drivers.IdDriver)].ToString().Trim()),
                            FirstName = reader[nameof(Drivers.FirstName)].ToString().Trim(),
                            MiddleName = reader[nameof(Drivers.MiddleName)].ToString().Trim(),
                            SecondName = reader[nameof(Drivers.SecondName)].ToString().Trim(),
                            DateOfBirth = DateTime.Parse(reader[nameof(Drivers.DateOfBirth)].ToString().Trim())
                        });
                    }

                    reader.Close();
                }

                return result;
            });
        }

        public void Update (Drivers model)
        {
            this.ExecuteNonQuery(
                $@"UPDATE {nameof(Drivers)} 
                SET {nameof(Drivers.FirstName)} = '{model.FirstName}',
                {nameof(Drivers.MiddleName)} = '{model.MiddleName}',
                {nameof(Drivers.SecondName)} = '{model.SecondName}',
                {nameof(Drivers.DateOfBirth)} = '{model.DateOfBirth}'
                WHERE {nameof(Drivers.IdDriver)} = {model.IdDriver}
            ");
        }
    }   
}
