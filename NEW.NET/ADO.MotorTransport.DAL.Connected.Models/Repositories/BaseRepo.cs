﻿using System;
using ADO.MotorTransport.DAL.Connected.Models.Extensions;
using System.Data.SqlClient;
using System.Configuration;

namespace ADO.MotorTransport.DAL.Connected.Models.Repositories
{
    public class BaseRepo
    {
        protected delegate void ExecuteWithConnectionNonQueryDelegate(SqlConnection connection);

        protected const string CONNECTION_STRING_NAME = "ADOConnection";

        protected string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings[CONNECTION_STRING_NAME].ToString();
            }
        }

        protected TReturnType ExecuteWithConnection<TReturnType>(Func<SqlConnection, TReturnType> callback)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                try
                {
                    cn.Open();
                    return callback(cn);
                }
                catch (SqlException)
                {
                    throw;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }
        }

        protected void ExecuteNonQuery(string query)
        {
            this.ExecuteWithConnection((SqlConnection connection) =>
            {
                connection.GetNonQueryCommand(query).ExecuteNonQuery();
            });
        }

        protected void ExecuteWithConnection(ExecuteWithConnectionNonQueryDelegate callback)
        {
            using (SqlConnection cn = new SqlConnection(this.ConnectionString))
            {
                try
                {
                    cn.Open();
                    callback(cn);
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    cn.Close();
                }
            }
        }
    }
 
}
