﻿using System;
using ADO.MotorTransport.DAL.Connected.Models;
using ADO.MotorTransport.DAL.Connected.Models.Repositories;
using System.Data.SqlClient;
using ADO.MotorTransport.DAL.Connected.Models.ADO.MotorTransport.DAL.Connected.Models;
using System.Collections.Generic;

namespace NEW.NET
{
    class Program
    {
        static void Main(string[] args)
        {
            //var vehicledeal = new VehiclesRepo();
            //vehicledeal.Add(new VehiclesNames { Manufacturer = "KRAZ", Model = "Sylach" });
            //vehicledeal.Delete(47);
            //vehicledeal.Update(new VehiclesNames { IdVehicleName = 49, Manufacturer = "URAL", Model = "Chovgan" });

            //var category = new CategoryRepo();
            //category.Add(new DriveCategory { ClassCategory = "DE", CategoryDescription = "Bus with a trailer less than 750kg."});
            //category.Add(new DriveCategory { ClassCategory = "DA", CategoryDescription = "Bus with a trailer more than 750kg." });
            //category.Delete("DA");
            //category.Update(new DriveCategory { ClassCategory = "DE", CategoryDescription = "Bus with a trailer more than 750kg." });

            //var driver = new DriversRepo();
            //driver.Add(new Drivers { FirstName = "Dmytro", MiddleName = "Ivanovych", SecondName = "Nolik", DateOfBirth = DateTime.Parse("1988-12-12") });
            //driver.Delete(20);
            //driver.Update(new Drivers { IdDriver = 21, FirstName = "Tolik", MiddleName = "Ivanovych", SecondName = "Rodik", DateOfBirth = DateTime.Parse("1999-10-12") });

            var fetch = new FetchSet();
            IEnumerable<FetchFromAll> derive = fetch.Get();
            foreach (FetchFromAll n in derive)
            {
                Console.WriteLine($"{n.Manufacturer, -14}, {n.Model, -15}, {n.FirstName, -10}, {n.LastName, -12}, {n.ClassCategory, -2}, {n.CategoryDescription, -20}");
            }
            Console.WriteLine();

            Console.Read();
        }
    }
}
