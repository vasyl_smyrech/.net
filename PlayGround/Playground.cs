﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        //int[] arreyLed = new int[5] { 5, 6, 8, 1, 7 };
        //Console.WriteLine (arreyLed [3]);
        //Console.ReadKey();
        //List<int> krem = new List<int>();
        //krem.Add(25);
        //krem.Add(26);
        //krem.Add(27);
        //krem.Add(28);
        //krem.Add(29);
        //for (int i = 0; i < krem.Count; i++)
        //{
        //  Console.WriteLine(krem[i] + " ;");
        //}
        //Console.ReadKey(); 
        /////////////multiplication plate 10x10//////////////////
        //int[,] arrey1 = new int[11, 11];
        //for(int i=0; i < arrey1.GetLength(0); i++)
        //{
        //    for (int j=0; j < arrey1.GetLength(1); j++)
        //    {
        //         arrey1[i,j] = i * j;
        //    }
        //}
        //for (int i = 1; i < arrey1.GetLength(0); i++)
        //{
        //    for (int j = 1; j < arrey1.GetLength(1); j++)
        //    {
        //        Console.WriteLine(i + " x " + j + " = " + arrey1[i, j]);
        //    }
        //    Console.WriteLine("\n");
        //}
        //Console.ReadKey();
        /////////////////////////////////////////////////////////
        class Student
        {
            public static int countOfStudents = 0;
            private string name;
            public string Name { get; set; }
            private int course;
            public int Course
            {
                get
                {
                    return course;
                }
                set
                {
                    if (value < 1)
                    {
                        course = 1;
                    }
                    else if (value >= 5)
                    {
                        course = 5;
                    }
                    else
                    {
                        course = value;
                    }
                }
            }
            private bool scholarship;
            private string scholarshipYN;

            public Student(string name, int course, bool scholarship)
            {
                this.name = name;
                this.course = course;
                this.scholarship = scholarship;
                countOfStudents++;
                courseCheck();
                printInfo();
            }



            /// <summary>
            /// Next method changes value of student scholarship from "true/false" to "yes/no".
            /// </summary>
            private void defscholar()
            {
                if (scholarship == true)
                {
                    scholarshipYN = "Yes";
                }
                else
                {
                    scholarshipYN = "No";
                }
            }
            /// <summary>
            /// Next method derive valid number of courses (1-5).
            /// </summary>
            private void courseCheck()
            {
                if (course < 1)
                {
                    this.course = 1;
                }
                else if (course >= 5)
                {
                    this.course = 5;
                }
            }
            public void printInfo()
            {

                defscholar();
                Console.WriteLine("\n Student name: " + name + ";\n Course: " + course + ";\n Scholarship: " + scholarshipYN + ";");
            }

        }

        class Mathematic : Student
        {
            float averageScore;
            string branch;
            int numberOfBranch;
            public Mathematic(string name, int course, bool scholarship, float averageScore, int branch) : base(name, course, scholarship)
            {
                this.averageScore = averageScore;
                if (branch <= 1)
                {
                    this.branch = "Geometry";
                }
                else
                {
                    this.branch = "Algebra";
                }
                Console.WriteLine(" Average score: " + averageScore + ";\n Branch: " + this.branch + ";\n");

            }
        }
        static void Main(string[] args)
        {
            var sergey = new Mathematic("Sergey", 4, true, 25.3f, 1);
            var ivan = new Student("Ivan", 0, true);
            var oleg = new Student("Oleg", 3, false);
            oleg.Name = "Olegek";
            Console.WriteLine(oleg.Name);
            var nikolay = new Student("Nikolay", 4, true);
            var trofim = new Student("Trofim", 10, false);
            Console.WriteLine("Total number of students: " + Student.countOfStudents);
            Console.ReadKey();
        }

    }
}

