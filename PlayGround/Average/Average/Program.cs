﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Average
{
    class Program
    {
        static void Main(string[] args)
        {
            List<double> inputNumbers = new List<double>();
            Console.WriteLine(" Enter number, then press 'Enter'. Repeat it as many times you want. \n If you want derive average from entered numbers, just press 'Enter'.");
            for (int i = 0; i <= inputNumbers.LongCount() ; i++)
            {
                var n = Console.ReadLine();
                if (n != "")
                {
                    double nparsed = double.Parse(n);
                    inputNumbers.Add(nparsed);
                }
                if (n == "")
                {
                    //Average - using build-in method
                    double averageOfinputNumbers; //= inputNumbers.Average();
                    //Average - using own method
                    double sumOfNumbers = 0;
                    int count = inputNumbers.Count;
                    foreach (double num in inputNumbers)
                    {
                        sumOfNumbers += num;
                    }
                    averageOfinputNumbers = sumOfNumbers / inputNumbers.Count;


                    Console.WriteLine(" Count of entered numbers is: " + inputNumbers.Count);
                    Console.WriteLine("\n Average for entered numbers is: " + averageOfinputNumbers);
                }

            }
            
            Console.ReadLine();
        }
    }
}
