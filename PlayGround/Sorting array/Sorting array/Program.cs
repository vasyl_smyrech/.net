﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting_array
{
    class Program
    {
        static void Main(string[] args)

        {   //input data
            double temporary;
            double[] sortingNums = new double[10];

            Console.WriteLine("\n\tEnter 10 integer or decimal numbers. After typing each number press 'Enter': ");
            for (int a = 0; a < sortingNums.Length; a++)
            {
                sortingNums[a] = double.Parse(Console.ReadLine());
            }

            //sorting
            for (int i = 0; i < sortingNums.Length; i++)
            {
                for (int j = i + 1; j < sortingNums.Length; j++)
                {
                    if (sortingNums[j] > sortingNums[i])
                    {
                        temporary = sortingNums[i];
                        sortingNums[i] = sortingNums[j];
                        sortingNums[j] = temporary;

                    }

                }
            }
            foreach (double num in sortingNums)
                {
                    Console.WriteLine(num);
                }

                Console.ReadKey();
            
        }
    }
}
