﻿using System;
using Calculator.BL;

namespace ConsoleCalc.App
{
    class ConsoleManager
    {
        
        OutputData outputText = new OutputData();
        private string first;
        private string second;
        // 2 items - output numbers from TryParse entering numbers      
        private string operation;
        private bool answerNum;
        public void Gritings()
        {
            outputText.OnGotMessage("\n\t\t\t\t\tI GREETING YOU IN MY CALCULATOR!!!");
            InitDialog();
        }
        /// <summary>
        /// Select the type of numbers the user want to deal.
        /// </summary>
        private void InitDialog()
        {
            outputText.OnGotMessage("\n\t\t\tEnter an action number you want to do than press 'Enter'.\n\n" +
                "\t\t1 - Operations with complex numbers;\t2 - Operations with decimals;\t3 - Exit.");
            InnerInitDialog();
        }
        private void InnerInitDialog()
        {
            Console.Write("\t");
            string decimalOrComplexAnswer = Console.ReadLine();
            if (decimalOrComplexAnswer == "1")
            {
                DealWithNumbers("complex");
            }
            else if (decimalOrComplexAnswer == "2")
            {
                DealWithNumbers("decimal");
            }
            else if (decimalOrComplexAnswer == "3") { }
            else
            {
                outputText.OnGotMessage("\n\tYou Entered invalid data. Enter number 1, 2 or 3 then press 'Enter':   ");
                InnerInitDialog();
            }
        }
        /// <summary>
        /// The method defines first, second and operation. Includes validation input data.
        /// </summary>
        /// <param name="typeOfNumber">Type the user chose at the InitDialog</param>
        private void DealWithNumbers(string typeOfNumber)
        {
            outputText.OnGotMessage($"\n\tEnter first {typeOfNumber} number, then press 'Enter': ");
            EnteringFirstNumber(typeOfNumber);
            outputText.OnGotMessage("\n\tEnter a sign of operation (+ , - , * , /) : ");
            TryOperation();
            outputText.OnGotMessage($"\n\tEnter second {typeOfNumber} number, then press 'Enter': ");
            EnteringSecondNumber(typeOfNumber);
            PrintAnswer(typeOfNumber);
        }
        //first number
        private void EnteringFirstNumber(string typeOfNum)
        {
            Console.Write("\t");
            string firstNumberString = Console.ReadLine();
            ParsebleOrNot(typeOfNum, firstNumberString);
            if (answerNum == true)
            {
                first = firstNumberString;
            }
            else if (answerNum == false)
            {
                EnteringFirstNumber(typeOfNum);
            }
        }
        //operation sign
        private void TryOperation()
        {
            Console.Write("\t");
            string tryoperation = Console.ReadLine();
            if (tryoperation == "+" || tryoperation == "-" || tryoperation == "*" || tryoperation == "/")
            {
                operation = tryoperation;
            }
            else
            {
                outputText.OnGotMessage("\n\tYou entered an invalid operation sign. Enter + , - , * or / , then press 'Enter':");
                TryOperation();
            }
        }
        //second number
        private void EnteringSecondNumber(string typeOfNum)
        {
            Console.Write("\t");
            string secondNumberString = Console.ReadLine();
            if (ParsebleOrNot(typeOfNum, secondNumberString) == true) 
            {
                second = secondNumberString;
            }
            else
            {
                EnteringSecondNumber(typeOfNum);
            }
        }
        /// <summary>
        /// Defines posibility to parse entered data.
        /// </summary>
        /// <param name="typeOfNumber"></param>
        /// <param name="numString">Number entered by the user (firstNumberString or secondNumberString).</param>
        /// <returns></returns>
        bool ParsebleOrNot(string typeOfNumber, string numString)
        {
            if (typeOfNumber == "complex")
            {
                ComplexNumbersOperations cn = new ComplexNumbersOperations();
                cn.GotMessage += outputText.OnGotMessage;
                answerNum = cn.ParseComplexNumber(numString, out double num1, out double num2);
                return answerNum;
            }
            else if (typeOfNumber == "decimal")
            {
                DecimalOperations dn = new DecimalOperations();
                dn.GotMessage += outputText.OnGotMessage;
                answerNum = dn.ParseDoubleNumber(numString, out double numOut);
                return answerNum;
            }
            else return false;
        }
        private void PrintAnswer(string typeOfNum)
        {
            OperationCaseResult(typeOfNum, operation);
            outputText.OnGotMessage("\n\n\tIf you want to continue computing enter 1 if not enter 2, then press 'Enter':  ");
            string exitAnswer = Console.ReadLine();
            if (exitAnswer == "1") InitDialog();
            else { }       
        }
        //creating instance of appropriate class and calling appropriate calculation
        private void OperationCaseResult(string typeOfNum, string operationSign)
        {
            if (typeOfNum == "complex")
            {
                ComplexNumbersOperations complexCalc = new ComplexNumbersOperations(first, second);
                complexCalc.GotResult += outputText.OnGotResult;
                complexCalc.GotMessage += outputText.OnGotMessage;
                OperAnswer(complexCalc);
            }
            else if (typeOfNum == "decimal")
            {
                DecimalOperations doubleCalc = new DecimalOperations(first, second);
                doubleCalc.GotResult += outputText.OnGotResult;
                doubleCalc.GotMessage += outputText.OnGotMessage;
                OperAnswer(doubleCalc);
            }
        }

        void OperAnswer(ICalck oper)
        {
            switch (operation)
            {
                case "+":
                    oper.Add();
                    break;
                case "-":
                    oper.Subtract();
                    break;
                case "*":
                    oper.Multiple();
                    break;
                case "/":
                    oper.Divide();
                    break;
            }
        }
    }
}