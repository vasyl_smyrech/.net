﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.BL
{
    public interface IOutputDataAndMassages
    {
        void ShowResult(string result);
        void ShowMessage(string message);
        void ShowError(string error);
    }

    public class OutputData : IOutputDataAndMassages
    {
        public void ShowResult(string result)
        {
            Console.WriteLine("\n\tThe answer is: " + result);
        }
        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
        public void ShowMessage(string message, string insertInMessage)
        {
            Console.WriteLine(message, insertInMessage);
        }
        public void ShowError(string error)
        {
            Console.WriteLine(error);
        }
    }
}
