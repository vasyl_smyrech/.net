﻿using System;


namespace Calculator.BL
{
    //Common abstract class for all type calculation
    public abstract class OperationCommon
    {
        protected string first;
        protected string second;
        public OperationCommon(string first, string second)
        {
            this.first = first;
            this.second = second;
        }
        public abstract string Add();
        public abstract string Divide();
        public abstract string Multiple();
        public abstract string Subtract();
    }
    //class for the case if user choose decimall. Cause user may not know what means 'double'
    public class OperationsWithDouble : OperationCommon
    {
        public OperationsWithDouble(string first, string second) : base(first, second)
        {
        }
        //parsing to double
        private double NumFirst()
        {
            double firstDouble = Double.Parse(first);
            return firstDouble;
        }
        private double NumSecond()
        {
            double secondInt = Double.Parse(second);
            return secondInt;
        }
        //operations
        public override string Add()
        {
            return (NumFirst() + NumSecond()).ToString();
        }
        public override string Divide()
        {
            return (NumFirst() / NumSecond()).ToString();
        }
        public override string Multiple()
        {
            return (NumFirst() * NumSecond()).ToString();
        }
        public override string Subtract()
        {
            return (NumFirst() - NumSecond()).ToString();
        }
    }
    //class for integer numbers
    public class OperationsWithInteger : OperationCommon
    {
        public OperationsWithInteger(string first, string second) : base(first, second)
            {
            }
        //parsing to integer
        private int NumFirst()
        {
            int firstInt = Int32.Parse(first);
            return firstInt;
        }
        private int NumSecond()
        {
            int secondInt = Int32.Parse(second);
            return secondInt;
        }
        //operations
        public override string Add()
            {
                return (NumFirst() + NumSecond()).ToString();
            }
        public override string Divide()
            {
                return (NumFirst() / NumSecond()).ToString();
            }
        public override string Multiple()
            {
                return (NumFirst() * NumSecond()).ToString();
            }
        public override string Subtract()
            {
                return (NumFirst() - NumSecond()).ToString();
            }
    }
}
