﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator.BL
{
    public interface IOutputDataAndMassages
    {
        void OnGotResult(object source, ResultOrMessageEventArgs e);
        void OnGotMessage(object source, ResultOrMessageEventArgs e);
    }
    public class OutputData : IOutputDataAndMassages
    {
        public void OnGotResult(object source, ResultOrMessageEventArgs result)
        {
            Console.WriteLine("\n\tThe answer is: " + result.Result);
        }
        public void OnGotMessage(string e)
        {
            Console.WriteLine(e);
        }
        public void OnGotResult(string result)
        {
            Console.WriteLine("\n\tThe answer is: " + result);
        }
        public void OnGotMessage(object source, ResultOrMessageEventArgs e)
        {
            Console.WriteLine(e.Result);
        }
        public void ShowError(string error)
        {
            Console.WriteLine(error);
        }
    }
}
