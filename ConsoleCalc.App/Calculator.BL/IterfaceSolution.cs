﻿using System;

namespace Calculator.BL
{
    public delegate void OutputEventHandler(object obj, ResultOrMessageEventArgs args);   
    public interface ICalck
    {
        string First { get; set; }  
        string Second { get; set; }
        void Add();
        void Divide();
        void Multiple();
        void Subtract();
    }
    public class DecimalOperations : ICalck
    {
        protected string first;
        public string First
        {
            get { return first; }
            set { first = value; }
        }
        protected string second;
        public string Second
        {
            get { return second; }
            set { second = value; }
        }
        public DecimalOperations() { }
        public DecimalOperations(string first, string second)
        {
            this.first = first;
            this.second = second;
        }
        //parsing to double
        public bool ParseDoubleNumber(string number, out double numOut)
        {
            bool parseCheck = Double.TryParse(number, out numOut);
            if (!parseCheck) { OnGotMessage("\n\tYou Entered invalid data. Enter decimal number then press 'Enter': ");} 
            return parseCheck;
        }
        //operations
        public void Add()
        {
            ParseDoubleNumber(first, out double numOne);
            ParseDoubleNumber(second, out double numTwo);
            OnGotResult((numOne + numTwo).ToString());
        }
        public void Divide()
        {
            ParseDoubleNumber(first, out double numOne);
            ParseDoubleNumber(second, out double numTwo);
            if (numTwo == 0)
            {
                OnGotMessage("\n\tYou can't divide by zero! Try again: ");
            }
            else { OnGotResult((numOne / numTwo).ToString()); }
        }
        public void Multiple()
        {
            ParseDoubleNumber(first, out double numOne);
            ParseDoubleNumber(second, out double numTwo);
            OnGotResult((numOne * numTwo).ToString());
        }
        public void Subtract()
        {
            ParseDoubleNumber(first, out double numOne);
            ParseDoubleNumber(second, out double numTwo);
            OnGotResult((numOne - numTwo).ToString());
        }
        //two different event needed in case it would be winform app and messages and results 
        //will be deriving in different textboxes
        public event OutputEventHandler GotResult;
        public event OutputEventHandler GotMessage;
        public virtual void OnGotResult(string result)
        {
            GotResult?.Invoke(this, new ResultOrMessageEventArgs() {Result = result});
        }
        public virtual void OnGotMessage(string message)
        {
            GotMessage?.Invoke(this, new ResultOrMessageEventArgs() { Result = message });
        }

    }
    public class ComplexNumbersOperations : ICalck
    {
        protected string first;
        public string First
        {
            get { return first; }
            set { first = value; }
        }
        protected string second;
        public string Second
        {
            get { return second; }
            set { second = value; }
        }
        private readonly double numOneA;
        private readonly double numOneB;
        private readonly double numTwoA;
        private readonly double numTwoB;
        public ComplexNumbersOperations() { }
        public ComplexNumbersOperations(string first, string second)
        {
            this.first = first;
            this.second = second;
            ParseComplexNumber(first, out numOneA, out numOneB);
            ParseComplexNumber(second, out numTwoA, out numTwoB);
        }
        public bool ParseComplexNumber(string number, out double num1, out double num2)
        {
            //trims whitespaces and splits onto two numbers
            number = number.Replace(" ", "");
            number = number.Replace("i", "");
            number = number.Replace("-", "#-");
            number = number.Replace("+", "#+");
            String[] num = number.Split('#');
            int mod = 0;
            if (num[0] == "") mod = 1;
            num1 = 0;
            num2 = 0;
            try
            {
                bool numOneCheck = double.TryParse(num[0 + mod], out double numOne);
                num1 = numOne;
                bool numTwoCheck = double.TryParse(num[1 + mod], out double numTwo);
                num2 = numTwo;
                return true;
            }
            catch (Exception)
            {
                OnGotMessage("\tComplex number must be in following format -5 - 8i. Try again:");
                return false;
            }
        }       
        //operations
        public void Add()
        {
            double real = numOneA + numTwoA;
            double imagin = numOneB + numTwoB;
            OnGotResult(ComplexAnswer(real, imagin));
        }
        public void Subtract()
        {
            double real = numOneA - numTwoA;
            double imagin = numOneB - numTwoB;
            OnGotResult(ComplexAnswer(real, imagin));
        }
        public void Divide()
        {
            double real = (numOneA * numTwoA + numOneB * numTwoB) / (numTwoA * numTwoA + numTwoB * numTwoB);
            double imagin = (numOneB * numTwoA - numOneA * numTwoB) / (numTwoA * numTwoA + numTwoB * numTwoB);
            OnGotResult(ComplexAnswer(real, imagin));
        }
        public void Multiple()
        {
            double real = numOneA * numTwoA - numOneB * numTwoB;
            double imagin = numOneA * numTwoB + numOneB * numTwoA;
            OnGotResult(ComplexAnswer(real, imagin));
        }
        private string ComplexAnswer(double num1, double num2)
        {
            if (num2 >= 0) return num1.ToString() + " + " + num2.ToString() + "i";
            else return num1.ToString() + " - " + num2.ToString().Trim('-') + "i";
        }
        public event OutputEventHandler GotResult;
        public event OutputEventHandler GotMessage;
        protected virtual void OnGotResult(string result)
        {
            GotResult?.Invoke(this, new ResultOrMessageEventArgs() { Result = result });
        }
        protected virtual void OnGotMessage(string message)
        {
            GotMessage?.Invoke(this, new ResultOrMessageEventArgs() { Result = message });
        }
    }
    public class ResultOrMessageEventArgs : EventArgs
    {
        public string Result { get; set; }
    }
}  
