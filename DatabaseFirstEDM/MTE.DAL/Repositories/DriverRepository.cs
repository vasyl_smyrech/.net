﻿using System;
using System.Collections.Generic;
using System.Linq;
using MTE.DAL.Interfaces.Interfaces;
using MTE.DAL.Model;
using MTE.DAL.DTOs;

namespace MTE.DAL.Repositories
{
    public class DriverRepository : IRepository<Driver, IDriver>
    {
        public int Insert(IDriver model)
        {
            var driver = ToEntity(model);

            using (var context = new MotorTransportEnterpriseEntities())
            {
                context.Drivers.Add(driver);
                context.SaveChanges();
            }
            return driver.Id;
        }
        public bool Insert(IEnumerable<IDriver> model)
        {
            var driverEntities = model
               .Select(ToEntity);
            bool result = false;

            using (var context = new MotorTransportEnterpriseEntities())
            {
                context.Drivers.AddRange(driverEntities);
                result = context.SaveChanges() > 0;
            }
            return result;
        }
        public void Update(IDriver model)
        {
            using (var context = new MotorTransportEnterpriseEntities())
            {
                var modelToUpdate = context.Drivers
                    .SingleOrDefault(o => model.Id.Equals(o.Id));
                if (modelToUpdate != null)
                {
                    modelToUpdate.FirstName = model.FirstName;
                    modelToUpdate.LastName = model.LastName;
                    modelToUpdate.DateOfBirth = model.DateOfBirth;
                }
                context.SaveChanges();
            }
        }
        public void Delete(int id)
        {
            using (var context = new MotorTransportEnterpriseEntities())
            {
                var driver = context.Drivers.FirstOrDefault(o => o.Id.Equals(id));

                if (driver != null)
                {
                    context.Drivers.Remove(driver);
                }

                context.SaveChanges();
            }
        }

        public IEnumerable<IDriver> Get(Func<Driver, bool> predicate)
        {
            var result = new List<DriverDto>();

            using (var context = new MotorTransportEnterpriseEntities())
            {
                result = context.Drivers.Where(predicate)
                    .Select(o => new DriverDto
                    {
                        DateOfBirth = o.DateOfBirth,
                        FirstName = o.FirstName,
                        Id = o.Id,
                        LastName = o.LastName
                    }).ToList();
            }

            return result;
        }
        public IEnumerable<DriverExtendedDto> Get()
        {
            var result = new List<DriverExtendedDto>();
            using (var context = new MotorTransportEnterpriseEntities())
            {
                
                result = (from d in context.Drivers
                            join c in context.DriveCategories on d.CategoryId equals c.ClassCategoryId
                            join v in context.VehiclesNames on c.ClassCategoryId equals v.CategoryId
                            select new DriverExtendedDto
                            {
                               Id = d.Id,
                               VehicleManufacturer = v.Manufacturer,
                               VehicleModel = v.Model,
                               FirstName = d.FirstName,
                               LastName = d.LastName
                            }).ToList();
               
            }
            return result;
        }
        private Driver ToEntity(IDriver model)
        {
            return new Driver
            {
                Id = model.Id,
                FirstName = model.FirstName,
                LastName = model.LastName,
                DateOfBirth = model.DateOfBirth
            };
        }
    }
}