﻿using System;
using System.Collections.Generic;
using System.Linq;
using MTE.DAL.Interfaces.Interfaces;
using MTE.DAL.Model;
using MTE.DAL.DTOs;
using MTE.DAL.Interfaces.Interfaces.ModelInterfaces;

namespace MTE.DAL.Repositories
{
    public class CategoryRepository : IRepository<DriveCategory, ICategory>
    {
        public int Insert(ICategory model)
        {
            var category = ToEntity(model);

            using (var context = new MotorTransportEnterpriseEntities())
            {
                context.DriveCategories.Add(category);
                context.SaveChanges();
            }

            return category.ClassCategoryId;
        }
        public bool Insert(IEnumerable<ICategory> model)
        {
            var categoryEntities = model.Select(ToEntity);

            bool result = false;

            using (var context = new MotorTransportEnterpriseEntities())
            {
                context.DriveCategories.AddRange(categoryEntities);
                result = context.SaveChanges() > 0;
            }

            return result;
        }
        public void Update(ICategory model)
        {
            using (var context = new MotorTransportEnterpriseEntities())
            {
                var modelToUpdate = context.DriveCategories
                    .SingleOrDefault(o => model.ClassCategoryId.Equals(o.ClassCategoryId));
                if (modelToUpdate != null)
                {
                    modelToUpdate.ClassCategory = model.ClassCategory;
                    modelToUpdate.CategoryDescription = model.CategoryDescription;
                }

                context.SaveChanges();
            }
        }
        public void Delete(int id)
        {
            using (var context = new MotorTransportEnterpriseEntities())
            {
                var category = context.DriveCategories.FirstOrDefault(o => o.ClassCategoryId.Equals(id));

                if (category != null)
                {
                    context.DriveCategories.Remove(category);
                }

                context.SaveChanges();
            }
        }

        public IEnumerable<ICategory> Get(Func<DriveCategory, bool> predicate)
        {
            var result = new List<CategoryDto>();

            using (var context = new MotorTransportEnterpriseEntities())
            {
                result = context.DriveCategories.Where(predicate).Select(o => new CategoryDto
                    {
                        ClassCategoryId = o.ClassCategoryId,
                        ClassCategory = o.ClassCategory,
                        CategoryDescription = o.CategoryDescription
                    }).ToList();
            }

            return result;
        }
        public IEnumerable<CategoryExtendedDto> Get()
        {
            using (var context = new MotorTransportEnterpriseEntities())
            {
                var result = new List<CategoryExtendedDto>();
                var middleResult = from c in context.DriveCategories
                                   join d in context.Drivers on c.ClassCategoryId equals d.CategoryId
                                   select new
                                   {
                                       c.ClassCategoryId,
                                       c.ClassCategory,
                                       c.CategoryDescription,
                                       d.FirstName,
                                       d.LastName
                                   };
            
                foreach (var item in middleResult)
                {
                    result.Add(new CategoryExtendedDto
                    {
                        ClassCategoryId = item.ClassCategoryId,
                        ClassCategory = item.ClassCategory,
                        CategoryDescription = item.CategoryDescription,
                        DriverFirstName = item.FirstName,
                        DriverLastName = item.LastName
                    });
                }
                return result;
            }
        }
        private DriveCategory ToEntity(ICategory model)
        {
            return new DriveCategory
            {
                ClassCategory = model.ClassCategory,
                CategoryDescription = model.CategoryDescription
            };
        }
    }
}

