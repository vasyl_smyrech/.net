﻿using System;
using System.Collections.Generic;
using System.Linq;
using MTE.DAL.Interfaces.Interfaces;
using MTE.DAL.Model;
using MTE.DAL.DTOs;

namespace MTE.DAL.Repositories
{
    public class VehicleRepository : IRepository<VehiclesName, IVehicle>
    {
        public int Insert(IVehicle model)
        {
            var vehicle = ToEntity(model);

            using (var context = new MotorTransportEnterpriseEntities())
            {
                context.VehiclesNames.Add(vehicle);
                context.SaveChanges();               
            }

            return vehicle.Id;
        }
        public bool Insert(IEnumerable<IVehicle> model)
        {
            var vehicleEntities = model.Select(ToEntity);

            bool result = false;

            using (var context = new MotorTransportEnterpriseEntities())
            {
                context.VehiclesNames.AddRange(vehicleEntities);
                result = context.SaveChanges() > 0;
            }

            return result;
        }
        public void Update(IVehicle model)
        {
            using (var context = new MotorTransportEnterpriseEntities())
            {
                var modelToUpdate = context.VehiclesNames.SingleOrDefault(o => model.Id.Equals(o.Id));
                if (modelToUpdate != null)
                {
                    modelToUpdate.CategoryId = model.CategoryId;
                    modelToUpdate.Manufacturer = model.Manufacturer;
                    modelToUpdate.Model = model.Model;
                }

                context.SaveChanges();
            }
        }
        public void Delete(int id)
        {
            using (var context = new MotorTransportEnterpriseEntities())
            {
                var vehicle = context.VehiclesNames.FirstOrDefault(o => o.Id.Equals(id));

                if (vehicle != null)
                {
                    context.VehiclesNames.Remove(vehicle);
                }
                context.SaveChanges();
            }
        }

        public IEnumerable<IVehicle> Get(Func<VehiclesName, bool> predicate)
        {
            var result = new List<VehicleDto>();

            using (var context = new MotorTransportEnterpriseEntities())
            {
                result = context.VehiclesNames.Where(predicate)
                    .Select(o => new VehicleDto
                    {
                        Manufacturer = o.Manufacturer,
                        Model = o.Model,
                        Id = o.Id,
                        CategoryId = o.CategoryId
                    }).ToList();
            }

            return result;
        }


        public IEnumerable<VehicleExtendedDto> Get()
        {
            var result = new List<VehicleExtendedDto>();
            using (var context = new MotorTransportEnterpriseEntities())
            {                
                var middleResult = (from v in context.VehiclesNames
                    join c in context.DriveCategories on v.CategoryId equals c.ClassCategoryId
                    join d in context.Drivers on c.ClassCategoryId equals d.CategoryId
                    select new VehicleExtendedDto
                    {
                        Id = v.Id,
                        Manufacturer = v.Manufacturer,
                        Model = v.Model,
                        ClassCategory = c.ClassCategory,
                        LastName = d.LastName
                    }).ToList();
                return result;
            }
        }
            
        private IVehicle EntityToDto(VehiclesName o)
        {
            throw new NotImplementedException();
        }

        private IVehicle EntityToDto(VehicleExtendedDto model)
        {
            return new VehicleExtendedDto
            {
                Id = model.Id,
                Manufacturer = model.Manufacturer,
                Model = model.Model,
                ClassCategory = model.ClassCategory,
                LastName = model.LastName
            };
        }
        private VehiclesName ToEntity(IVehicle model)
        {
            return new VehiclesName
            {
                Id = model.Id,
                Manufacturer = model.Manufacturer,
                Model = model.Model,
                CategoryId = model.CategoryId
            };
        }
    }
}
