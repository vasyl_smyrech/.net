﻿using MTE.DAL.Interfaces.Interfaces.ModelInterfaces;

namespace MTE.DAL.DTOs
{
    public class CategoryDto : ICategory
    {
       public int ClassCategoryId { get; set; }
       public string ClassCategory { get; set; }
       public string CategoryDescription { get; set; }
    }
}
