﻿using MTE.DAL.Interfaces.Interfaces;

namespace MTE.DAL.DTOs
{
    public class VehicleDto : IVehicle
    {
        public int Id { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public int CategoryId { get; set; }
    }
}
