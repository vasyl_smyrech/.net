﻿using System;
using MTE.DAL.Interfaces.Interfaces;

namespace MTE.DAL.DTOs
{
    public class DriverDto : IDriver
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public int CategoryId { get; set; }
    }
}
