﻿using MTE.DAL.Interfaces.Interfaces.ModelInterfaces;


namespace MTE.DAL.DTOs
{
    public class CategoryExtendedDto : ICategory
    {
        public int ClassCategoryId { get; set; }
        public string ClassCategory { get; set; }
        public string CategoryDescription { get; set; }
        public string DriverLastName { get; set; }
        public string DriverFirstName { get; set; }
    }
}
