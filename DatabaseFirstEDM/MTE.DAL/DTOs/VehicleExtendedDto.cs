﻿using MTE.DAL.Interfaces.Interfaces;

namespace MTE.DAL.DTOs
{
    public class VehicleExtendedDto : IVehicle
    {
        public string ClassCategory { get; set; }
        public int Id { get; set; }
        public string LastName { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public int CategoryId { get; set;}
    }
}
