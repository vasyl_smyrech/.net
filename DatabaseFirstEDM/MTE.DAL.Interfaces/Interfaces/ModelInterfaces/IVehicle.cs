﻿
namespace MTE.DAL.Interfaces.Interfaces
{
    public interface IVehicle
    {
        int Id { get; }
        string Manufacturer { get; }
        string Model { get; }
        int CategoryId { get; }
    }
}
