﻿using System;

namespace MTE.DAL.Interfaces.Interfaces
{
    public interface IDriver
    {
        int Id { get; }
        string FirstName { get; }
        string LastName { get; }
        Nullable<System.DateTime> DateOfBirth { get; }
        int CategoryId { get; }
    }
}
