﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTE.DAL.Interfaces.Interfaces.ModelInterfaces
{
    public interface ICategory
    {
        int ClassCategoryId { get;  }
        string ClassCategory { get; }
        string CategoryDescription { get; }
    }
}
