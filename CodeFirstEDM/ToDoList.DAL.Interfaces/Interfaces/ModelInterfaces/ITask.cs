﻿using System;

namespace ToDoList.DAL.Interfaces.ModelInterfaces
{
    public interface ITask
    {
        int Id { get; }
        string TaskTitle { get; }
        string TaskDescription { get; }
        string Type { get;  }
        DateTime? EstimateFinish { get; }
        double? WorkHoursRequaired { get; }
        string Status { get; }
        DateTime? Startedwork { get; }
        int? DonePercentage { get; }
    }
}

