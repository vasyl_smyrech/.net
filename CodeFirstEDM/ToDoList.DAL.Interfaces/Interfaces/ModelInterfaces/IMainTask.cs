﻿using ToDoList.DAL.Interfaces.ModelInterfaces;

namespace ToDoList.DAL.Interfaces.Interfaces.ModelInterfaces
{
    public interface IMainTask : ITask
    {
        int? TeamId { get; }
    }
}
