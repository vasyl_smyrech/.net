﻿namespace ToDoList.DAL.Interfaces.ModelInterfaces
{
    public interface ITeam
    {
        int Id { get; }
        string TimNickName { get; }
        int TeamLeadId { get; }
    }
}
