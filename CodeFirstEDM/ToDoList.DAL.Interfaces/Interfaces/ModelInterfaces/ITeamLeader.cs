﻿namespace ToDoList.DAL.Interfaces.ModelInterfaces
{
    public interface ITeamLeader
    {
        int TeamId { get; }
        int PerformerId { get; }
    }
}
