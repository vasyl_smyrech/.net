﻿namespace ToDoList.DAL.Interfaces.Interfaces.ModelInterfaces
{
    public interface IPerformerSmallTask
    {
        int PerformerId { get; }
        int SmallTaskId { get; }
    }
}
