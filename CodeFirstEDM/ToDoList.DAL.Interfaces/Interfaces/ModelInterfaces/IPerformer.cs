﻿namespace ToDoList.DAL.Interfaces.ModelInterfaces
{
    public interface IPerformer
    {
        int Id { get; }
        string FirstName { get; }
        string LastName { get; }
        string ProfesionalLevel { get; }
    }
}
