﻿using ToDoList.DAL.Interfaces.ModelInterfaces;

namespace ToDoList.DAL.Interfaces.Interfaces.ModelInterfaces
{
    public interface ISmallTask : ITask
    {
        int? MainTaskId { get; }
        string Remark { get; }
    }
}
