﻿using System;
using System.Collections.Generic;

namespace ToDoList.DAL.Interfaces.RepositoryInterfaces
{
    public interface IRepository<TEntity, TDto>
    {
        int Insert(TDto model);
        bool Insert(IEnumerable<TDto> model);
        void Update(TDto model);
        void Delete(int id);
        IEnumerable<TDto> Get(Func<TEntity, bool> predicate);
    }
}