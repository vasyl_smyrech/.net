using System.Data.Entity;
using ListToDo.DAL.DTOs;
using ToDoList.DTOs;

namespace ToDoList
{
    public class ToDoListContext : DbContext
    {

        public ToDoListContext()
            : base("name=ToDoList")
        {
        }

        public virtual DbSet<MainTask> MainTaskEntity { get; set; }
        public virtual DbSet<SmallTask> SmallTaskEntity { get; set; }
        public virtual DbSet<Performer> PerformerEntity { get; set; }
        public virtual DbSet<Team> TeamEntity { get; set; }
        public virtual DbSet<PerformerSmallTask> PerformerSmallTaskEntity { get; set;}


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<TeamLeader>()
            //    .HasRequired(s => s.Team)
            //    .WithRequiredPrincipal(tl => tl.TeamLeader);
            //modelBuilder.Entity<TeamLeader>()
            //    .HasOptional(s => s.Performer);
            //modelBuilder.Entity<SmallTask>().HasMany(c => c.Performers)
            //    .WithMany(s => s.SmallTasks)
            //    .Map(t => t.MapLeftKey("SmallTaskId")
            //    .MapRightKey("PerformerId")
            //    .ToTable("SmallTaskPerformer"));

            //modelBuilder.Entity<SmallTask>().HasMany(c => c.Performers)
            //   .WithMany(s => s.SmallTasks)
            //   .Map(t => t.MapLeftKey("SmallTaskId")
            //   .MapRightKey("PerformerId")
            //   .ToTable("SmallTaskPerformer"));
        }
    }
}