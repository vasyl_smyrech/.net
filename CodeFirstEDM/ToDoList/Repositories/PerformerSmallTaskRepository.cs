﻿using System;
using System.Collections.Generic;
using System.Linq;
using ToDoList.DTOs;
using ToDoList.DAL.Interfaces.RepositoryInterfaces;
using ToDoList.DAL.Interfaces.Interfaces.ModelInterfaces;


namespace ToDoList.DAL.Repositories
{
    public class PerformerSmallTaskRepository : IRepository<PerformerSmallTask, IPerformerSmallTask>
    {
        public int Insert(IPerformerSmallTask emploeeTask)
        {
            var performerSmallTask = ToEntity(emploeeTask);

            using (var context = new ToDoListContext())
            {
                context.PerformerSmallTaskEntity.Add(performerSmallTask);

                context.SaveChanges();
            }

            return emploeeTask.SmallTaskId;
        }

        public bool Insert(IEnumerable<IPerformerSmallTask> employeesTasks)
        {
            var performerSmallTaskEntities = employeesTasks
                .Select(ToEntity);

            bool result = false;

            using (var context = new ToDoListContext())
            {
                context.PerformerSmallTaskEntity.AddRange(performerSmallTaskEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id) { }

        public void Delete(int performerId, int smallTaskId)
        {
            using (var context = new ToDoListContext())
            {
                var performerSmallTask = context.PerformerSmallTaskEntity
                    .FirstOrDefault(o => o.SmallTaskId.Equals(smallTaskId) && o.PerformerId.Equals(performerId));

                if (performerSmallTask != null)
                {
                    context.PerformerSmallTaskEntity.Remove(performerSmallTask);
                }

                context.SaveChanges();
            }
        }

        public void Update(IPerformerSmallTask emploeeTask)
        {
        }


        public IEnumerable<IPerformerSmallTask> Get(Func<PerformerSmallTask, bool> predicate)
        {
            var result = new List<PerformerSmallTask>();

            using (var context = new ToDoListContext())
            {
                result = context.PerformerSmallTaskEntity.Where(predicate)
                    .Select(o => new PerformerSmallTask
                    {
                        PerformerId = o.PerformerId,
                        SmallTaskId = o.SmallTaskId
                    }).ToList();
            }

            return result;
        }

        public bool Any(Func<PerformerSmallTask, bool> predicate)
        {
            bool result = false;

            using (var context = new ToDoListContext())
            {
                result = context.PerformerSmallTaskEntity.Any(predicate);
            }

            return result;
        }

        private PerformerSmallTask ToEntity(IPerformerSmallTask emploeeTask)
        {
            return new PerformerSmallTask
            {
                PerformerId = emploeeTask.PerformerId,
                SmallTaskId = emploeeTask.SmallTaskId
            };
        }
    }
}