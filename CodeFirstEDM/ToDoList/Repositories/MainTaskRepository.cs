﻿using System;
using System.Collections.Generic;
using System.Linq;
using ListToDo.DAL.DTOs;
using ToDoList.DAL.Interfaces.RepositoryInterfaces;
using ToDoList.DAL.Interfaces.Interfaces.ModelInterfaces;
using ToDoList.DTOs;


namespace ToDoList.DAL.Repositories
{
    public class MainTaskRepository : IRepository<MainTask, IMainTask>
    {
        public int Insert(IMainTask mainTask)
        {
            var mainTaskEntity = ToEntity(mainTask);

            using (var context = new ToDoListContext())
            {
                context.MainTaskEntity.Add(mainTaskEntity);

                context.SaveChanges();
            }

            return mainTask.Id;
        }

        public bool Insert(IEnumerable<IMainTask> mainTasks)
        {
            var mainTaskEntities = mainTasks
                .Select(ToEntity);

            bool result = false;

            using (var context = new ToDoListContext())
            {
                context.MainTaskEntity.AddRange(mainTaskEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new ToDoListContext())
            {
                var mainTask = context.MainTaskEntity
                    .FirstOrDefault(o => o.Id.Equals(id));

                if (mainTask != null)
                {
                    context.MainTaskEntity.Remove(mainTask);
                }

                context.SaveChanges();
            }
        }

        public void Update(IMainTask mainTask)
        {
            using (var context = new ToDoListContext())
            {
                var modelToUpdate = context.MainTaskEntity
                    .SingleOrDefault(o => mainTask.Id.Equals(o.Id));
                if (modelToUpdate != null)
                {
                    modelToUpdate.TaskTitle = mainTask.TaskTitle;
                    modelToUpdate.TaskDescription = mainTask.TaskDescription;
                    modelToUpdate.WorkHoursRequaired = mainTask.WorkHoursRequaired;
                    modelToUpdate.DonePercentage = mainTask.DonePercentage;
                    modelToUpdate.EstimateFinish = mainTask.EstimateFinish;
                    modelToUpdate.Startedwork = mainTask.Startedwork;
                    modelToUpdate.Status = mainTask.Status;
                    modelToUpdate.WorkHoursRequaired = mainTask.WorkHoursRequaired;
                    modelToUpdate.TeamId = mainTask.TeamId;
                }

                context.SaveChanges();
            }
        }

        public IEnumerable<IMainTask> Get(Func<MainTask, bool> predicate)
        {
            var result = new List<MainTask>();

            using (var context = new ToDoListContext())
            {
                result = context.MainTaskEntity.Where(predicate)
                    .Select(o => new MainTask
                    {
                        TaskTitle = o.TaskTitle,
                        TaskDescription = o.TaskDescription,
                        WorkHoursRequaired = o.WorkHoursRequaired,
                        DonePercentage = o.DonePercentage,
                        EstimateFinish = o.EstimateFinish,
                        Startedwork = o.Startedwork,
                        Status = o.Status,
                        TeamId = o.TeamId
                    }).ToList();
            }

            return result;
        }

        public IEnumerable<MainTaskExtended> Get()
        {
            var result = new List<MainTaskExtended>();

            using (var context = new ToDoListContext())
            {
                result = (from m in context.MainTaskEntity
                          join p in context.PerformerEntity on m.Id equals p.Id
                          join ps in context.PerformerSmallTaskEntity on p.Id equals ps.PerformerId
                          join st in context.SmallTaskEntity on ps.SmallTaskId equals st.Id
                          select new MainTaskExtended
                          {
                               Id = m.Id,
                               TaskTitle = m.TaskTitle,
                               TaskDescription = m.TaskDescription,
                               Type = m.Type,
                               SmallTaskTitle = st.TaskTitle,
                               Performer = p.LastName
                          }).ToList();
            }

            return result;
        }

        public bool Any(Func<MainTask, bool> predicate)
        {
            bool result = false;

            using (var context = new ToDoListContext())
            {
                result = context.MainTaskEntity.Any(predicate);
            }

            return result;
        }

        private MainTask ToEntity(IMainTask mainTask)
        {
            return new MainTask
            {
                TaskTitle = mainTask.TaskTitle,
                TaskDescription = mainTask.TaskDescription,
                WorkHoursRequaired = mainTask.WorkHoursRequaired,
                DonePercentage = mainTask.DonePercentage,
                EstimateFinish = mainTask.EstimateFinish,
                Startedwork = mainTask.Startedwork,
                Status = mainTask.Status,
                TeamId = mainTask.TeamId
            };
        }
    }
}
