﻿using System;
using System.Collections.Generic;
using System.Linq;
using ListToDo.DAL.DTOs;
using ToDoList.DAL.Interfaces.RepositoryInterfaces;
using ToDoList.DAL.Interfaces.Interfaces.ModelInterfaces;


namespace ToDoList.DAL.Repositories
{
    public class SmallTaskRepository : IRepository<SmallTask, ISmallTask>
    {
        public int Insert(ISmallTask smallTask)
        {
            var smallTaskEntity = ToEntity(smallTask);

            using (var context = new ToDoListContext())
            {
                context.SmallTaskEntity.Add(smallTaskEntity);

                context.SaveChanges();
            }

            return smallTask.Id;
        }

        public bool Insert(IEnumerable<ISmallTask> smallTasks)
        {
            var smallTaskEntities = smallTasks
                .Select(ToEntity);

            bool result = false;

            using (var context = new ToDoListContext())
            {
                context.SmallTaskEntity.AddRange(smallTaskEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new ToDoListContext())
            {
                var smallTask = context.SmallTaskEntity
                    .FirstOrDefault(o => o.Id.Equals(id));

                if (smallTask != null)
                {
                    context.SmallTaskEntity.Remove(smallTask);
                }

                context.SaveChanges();
            }
        }

        public void Update(ISmallTask smallTask)
        {
            using (var context = new ToDoListContext())
            {
                var modelToUpdate = context.SmallTaskEntity
                    .SingleOrDefault(o => smallTask.Id.Equals(o.Id));
                if (modelToUpdate != null)
                {
                    modelToUpdate.TaskTitle = smallTask.TaskTitle;
                    modelToUpdate.TaskDescription = smallTask.TaskDescription;
                    modelToUpdate.WorkHoursRequaired = smallTask.WorkHoursRequaired;
                    modelToUpdate.DonePercentage = smallTask.DonePercentage;
                    modelToUpdate.EstimateFinish = smallTask.EstimateFinish;
                    modelToUpdate.Startedwork = smallTask.Startedwork;
                    modelToUpdate.Status = smallTask.Status;
                    modelToUpdate.WorkHoursRequaired = smallTask.WorkHoursRequaired;
                    modelToUpdate.MainTaskId = smallTask.MainTaskId;
                    modelToUpdate.Remark = smallTask.Remark;
                }

                context.SaveChanges();
            }
        }

        public IEnumerable<ISmallTask> Get(Func<SmallTask, bool> predicate)
        {
            var result = new List<SmallTask>();

            using (var context = new ToDoListContext())
            {
                result = context.SmallTaskEntity.Where(predicate)
                    .Select(o => new SmallTask
                    {
                        TaskTitle = o.TaskTitle,
                        TaskDescription = o.TaskDescription,
                        WorkHoursRequaired = o.WorkHoursRequaired,
                        DonePercentage = o.DonePercentage,
                        EstimateFinish = o.EstimateFinish,
                        Startedwork = o.Startedwork,
                        Status = o.Status,
                        MainTaskId = o.MainTaskId,
                        Remark = o.Remark
                    }).ToList();
            }

            return result;
        }

        public bool Any(Func<SmallTask, bool> predicate)
        {
            bool result = false;

            using (var context = new ToDoListContext())
            {
                result = context.SmallTaskEntity.Any(predicate);
            }

            return result;
        }

        private SmallTask ToEntity(ISmallTask smallTask)
        {
            return new SmallTask
            {
                TaskTitle = smallTask.TaskTitle,
                TaskDescription = smallTask.TaskDescription,
                WorkHoursRequaired = smallTask.WorkHoursRequaired,
                DonePercentage = smallTask.DonePercentage,
                EstimateFinish = smallTask.EstimateFinish,
                Startedwork = smallTask.Startedwork,
                Status = smallTask.Status,
                MainTaskId = smallTask.MainTaskId,
                Remark = smallTask.Remark
            };
        }
    }
}