﻿using System;
using System.Collections.Generic;
using System.Linq;
using ListToDo.DAL.DTOs;
using ToDoList.DAL.Interfaces.RepositoryInterfaces;
using ToDoList.DAL.Interfaces.ModelInterfaces;
using ToDoList.DTOs;


namespace ToDoList.DAL.Repositories
{
    public class PerformerRepository : IRepository<Performer, IPerformer>
    {
        public int Insert(IPerformer emploee)
        {
                var performer = ToEntity(emploee);

                using (var context = new ToDoListContext())
                {
                    context.PerformerEntity.Add(performer);

                    context.SaveChanges();
                }
           
            return emploee.Id;
        }

        public bool Insert(IEnumerable<IPerformer> employees)
        {
            var performerEntities = employees
                .Select(ToEntity);

            bool result = false;

            using (var context = new ToDoListContext())
            {
                context.PerformerEntity.AddRange(performerEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new ToDoListContext())
            {
                var performer = context.PerformerEntity
                    .FirstOrDefault(o => o.Id.Equals(id));

                if (performer != null)
                {
                    context.PerformerEntity.Remove(performer);
                }

                context.SaveChanges();
            }
        }

        public void Update(IPerformer emploee)
        {
            using (var context = new ToDoListContext())
            {
                var modelToUpdate = context.PerformerEntity
                    .SingleOrDefault(o => emploee.Id.Equals(o.Id));
                if (modelToUpdate != null)
                {
                    modelToUpdate.ProfesionalLevel = emploee.ProfesionalLevel;
                    modelToUpdate.FirstName = emploee.FirstName;
                    modelToUpdate.LastName = emploee.LastName;
                }

                context.SaveChanges();
            }
        }

        public IEnumerable<IPerformer> Get(Func<Performer, bool> predicate)
        {
            var result = new List<Performer>();

            using (var context = new ToDoListContext())
            {
                result = context.PerformerEntity.Where(predicate)
                    .Select(o => new Performer
                    {
                        Id = o.Id,
                        FirstName = o.FirstName,
                        LastName = o.LastName,
                        ProfesionalLevel = o.ProfesionalLevel
                    }).ToList();
            }

            return result;
        }

        public IEnumerable<PerformerExtended> Get()
        {
            var result = new List<PerformerExtended>();

            using (var context = new ToDoListContext())
            {
                result = (from p in context.PerformerEntity
                         join ps in context.PerformerSmallTaskEntity on p.Id equals ps.PerformerId
                         join st in context.SmallTaskEntity on ps.SmallTaskId equals st.Id
                         select new PerformerExtended
                    {
                        Id = p.Id,
                        FirstName = p.FirstName,
                        LastName = p.LastName,
                        ProfesionalLevel = p.ProfesionalLevel,
                        SmallTask = st.TaskTitle
                    }).ToList();
            }

            return result;
        }

        public bool Any(Func<Performer, bool> predicate)
        {
            bool result = false;

            using (var context = new ToDoListContext())
            {
                result = context.PerformerEntity.Any(predicate);
            }

            return result;
        }

        private Performer ToEntity(IPerformer emploee)
        {
            return new Performer
            {
                FirstName = emploee.FirstName,
                LastName = emploee.LastName,
                ProfesionalLevel = emploee.ProfesionalLevel
            };
        }
    }
}
