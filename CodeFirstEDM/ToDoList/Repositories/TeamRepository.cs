﻿using System;
using System.Collections.Generic;
using System.Linq;
using ListToDo.DAL.DTOs;
using ToDoList.DAL.Interfaces.RepositoryInterfaces;
using ToDoList.DAL.Interfaces.ModelInterfaces;
using ToDoList.DTOs;


namespace ToDoList.Repositories
{
    public class TeamRepository : IRepository<Team, ITeam>
    {
        public int Insert(ITeam team)
        {
            var performer = ToEntity(team);

            using (var context = new ToDoListContext())
            {
                context.TeamEntity.Add(performer);

                context.SaveChanges();
            }

            return team.Id;
        }

        public bool Insert(IEnumerable<ITeam> employees)
        {
            var performerEntities = employees
                .Select(ToEntity);

            bool result = false;

            using (var context = new ToDoListContext())
            {
                context.TeamEntity.AddRange(performerEntities);

                result = context.SaveChanges() > 0;
            }

            return result;
        }

        public void Delete(int id)
        {
            using (var context = new ToDoListContext())
            {
                var team = context.PerformerEntity
                    .FirstOrDefault(o => o.Id.Equals(id));

                if (team != null)
                {
                    context.PerformerEntity.Remove(team);
                }

                context.SaveChanges();
            }
        }

        public void Update(ITeam team)
        {
            using (var context = new ToDoListContext())
            {
                var modelToUpdate = context.TeamEntity
                    .SingleOrDefault(o => team.Id.Equals(o.Id));
                if (modelToUpdate != null)
                {
                    modelToUpdate.TeamLeadId = team.TeamLeadId;
                    modelToUpdate.TimNickName = team.TimNickName;
                }

                context.SaveChanges();
            }
        }

        public IEnumerable<ITeam> Get(Func<Team, bool> predicate)
        {
            var result = new List<Team>();

            using (var context = new ToDoListContext())
            {
                result = context.TeamEntity.Where(predicate)
                    .Select(o => new Team
                    {
                        Id = o.Id,
                        TimNickName = o.TimNickName,
                        TeamLeadId = o.TeamLeadId
                    }).ToList();
            }

            return result;
        }

        public IEnumerable<TeamExtended> Get()
        {
            var result = new List<TeamExtended>();

            using (var context = new ToDoListContext())
            {
                result = (from t in context.TeamEntity
                          join p in context.PerformerEntity on t.Id equals p.Id
                          join ps in context.PerformerSmallTaskEntity on p.Id equals ps.PerformerId
                          join mt in context.MainTaskEntity on p.Id equals mt.Id
                          select new TeamExtended
                          {
                              Id = t.Id,
                              TimNickName= t.TimNickName,
                              PerformerLastName = p.LastName,
                              MainTaskTitle = mt.TaskTitle
                          }).ToList();
            }

            return result;
        }

        public bool Any(Func<Team, bool> predicate)
        {
            bool result = false;

            using (var context = new ToDoListContext())
            {
                result = context.TeamEntity.Any(predicate);
            }

            return result;
        }

        private Team ToEntity(ITeam team)
        {
            return new Team
            {
                TimNickName = team.TimNickName,
                TeamLeadId = team.TeamLeadId
            };
        }
    }
    
}
