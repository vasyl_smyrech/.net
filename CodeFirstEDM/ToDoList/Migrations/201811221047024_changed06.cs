namespace ToDoList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changed06 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.SmallTasks", name: "MainTask_Id", newName: "MainTaskId");
            RenameIndex(table: "dbo.SmallTasks", name: "IX_MainTask_Id", newName: "IX_MainTaskId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.SmallTasks", name: "IX_MainTaskId", newName: "IX_MainTask_Id");
            RenameColumn(table: "dbo.SmallTasks", name: "MainTaskId", newName: "MainTask_Id");
        }
    }
}
