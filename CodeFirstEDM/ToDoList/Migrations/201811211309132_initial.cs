namespace ToDoList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MainTasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaskTitle = c.String(nullable: false, maxLength: 255),
                        TaskDeskription = c.String(maxLength: 510),
                        Type = c.String(maxLength: 12),
                        EstimateFinish = c.DateTime(nullable: false),
                        WorkHoursRequaired = c.Double(nullable: false),
                        Status = c.String(maxLength: 25),
                        Startedwork = c.DateTime(nullable: false),
                        DonePercentage = c.Int(nullable: false),
                        Team_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Teams", t => t.Team_Id)
                .Index(t => t.Team_Id);
            
            CreateTable(
                "dbo.SmallTasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TaskTitle = c.String(nullable: false, maxLength: 255),
                        TaskDeskription = c.String(nullable: false, maxLength: 510),
                        Type = c.String(maxLength: 25),
                        EstimateFinish = c.DateTime(nullable: false),
                        WorkHoursRequaired = c.Double(nullable: false),
                        Status = c.String(maxLength: 25),
                        Startedwork = c.DateTime(nullable: false),
                        DonePercentage = c.Int(nullable: false),
                        Remark = c.String(maxLength: 300),
                        MainTask_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.MainTasks", t => t.MainTask_Id)
                .Index(t => t.MainTask_Id);
            
            CreateTable(
                "dbo.Performers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 20),
                        LastName = c.String(nullable: false, maxLength: 25),
                        ProfesionalLevel = c.String(nullable: false, maxLength: 6),
                        Team_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Teams", t => t.Team_Id)
                .Index(t => t.Team_Id);
            
            CreateTable(
                "dbo.Teams",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TimNickName = c.String(nullable: false, maxLength: 20),
                        TeamLead_Id = c.Int(nullable: false),
                        Performer_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Performers", t => t.TeamLead_Id, cascadeDelete: true)
                .ForeignKey("dbo.Performers", t => t.Performer_Id)
                .Index(t => t.TeamLead_Id)
                .Index(t => t.Performer_Id);
            
            CreateTable(
                "dbo.PerformerSmallTasks",
                c => new
                    {
                        Performer_Id = c.Int(nullable: false),
                        SmallTask_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Performer_Id, t.SmallTask_Id })
                .ForeignKey("dbo.Performers", t => t.Performer_Id, cascadeDelete: true)
                .ForeignKey("dbo.SmallTasks", t => t.SmallTask_Id, cascadeDelete: true)
                .Index(t => t.Performer_Id)
                .Index(t => t.SmallTask_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Teams", "Performer_Id", "dbo.Performers");
            DropForeignKey("dbo.Teams", "TeamLead_Id", "dbo.Performers");
            DropForeignKey("dbo.Performers", "Team_Id", "dbo.Teams");
            DropForeignKey("dbo.MainTasks", "Team_Id", "dbo.Teams");
            DropForeignKey("dbo.PerformerSmallTasks", "SmallTask_Id", "dbo.SmallTasks");
            DropForeignKey("dbo.PerformerSmallTasks", "Performer_Id", "dbo.Performers");
            DropForeignKey("dbo.SmallTasks", "MainTask_Id", "dbo.MainTasks");
            DropIndex("dbo.PerformerSmallTasks", new[] { "SmallTask_Id" });
            DropIndex("dbo.PerformerSmallTasks", new[] { "Performer_Id" });
            DropIndex("dbo.Teams", new[] { "Performer_Id" });
            DropIndex("dbo.Teams", new[] { "TeamLead_Id" });
            DropIndex("dbo.Performers", new[] { "Team_Id" });
            DropIndex("dbo.SmallTasks", new[] { "MainTask_Id" });
            DropIndex("dbo.MainTasks", new[] { "Team_Id" });
            DropTable("dbo.PerformerSmallTasks");
            DropTable("dbo.Teams");
            DropTable("dbo.Performers");
            DropTable("dbo.SmallTasks");
            DropTable("dbo.MainTasks");
        }
    }
}
