namespace ToDoList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changed03 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.MainTasks", "EstimateFinish", c => c.DateTime());
            AlterColumn("dbo.MainTasks", "WorkHoursRequaired", c => c.Double());
            AlterColumn("dbo.MainTasks", "Startedwork", c => c.DateTime());
            AlterColumn("dbo.MainTasks", "DonePercentage", c => c.Int());
            AlterColumn("dbo.SmallTasks", "EstimateFinish", c => c.DateTime());
            AlterColumn("dbo.SmallTasks", "WorkHoursRequaired", c => c.Double());
            AlterColumn("dbo.SmallTasks", "Startedwork", c => c.DateTime());
            AlterColumn("dbo.SmallTasks", "DonePercentage", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SmallTasks", "DonePercentage", c => c.Int(nullable: false));
            AlterColumn("dbo.SmallTasks", "Startedwork", c => c.DateTime(nullable: false));
            AlterColumn("dbo.SmallTasks", "WorkHoursRequaired", c => c.Double(nullable: false));
            AlterColumn("dbo.SmallTasks", "EstimateFinish", c => c.DateTime(nullable: false));
            AlterColumn("dbo.MainTasks", "DonePercentage", c => c.Int(nullable: false));
            AlterColumn("dbo.MainTasks", "Startedwork", c => c.DateTime(nullable: false));
            AlterColumn("dbo.MainTasks", "WorkHoursRequaired", c => c.Double(nullable: false));
            AlterColumn("dbo.MainTasks", "EstimateFinish", c => c.DateTime(nullable: false));
        }
    }
}
