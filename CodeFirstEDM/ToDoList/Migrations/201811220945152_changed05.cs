namespace ToDoList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changed05 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.MainTasks", name: "Team_Id", newName: "TeamId");
            RenameIndex(table: "dbo.MainTasks", name: "IX_Team_Id", newName: "IX_TeamId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.MainTasks", name: "IX_TeamId", newName: "IX_Team_Id");
            RenameColumn(table: "dbo.MainTasks", name: "TeamId", newName: "Team_Id");
        }
    }
}
