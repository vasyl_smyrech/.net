namespace ToDoList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changed02 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Teams", name: "TeamLead_Id", newName: "TeamLeadId");
            RenameIndex(table: "dbo.Teams", name: "IX_TeamLead_Id", newName: "IX_TeamLeadId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Teams", name: "IX_TeamLeadId", newName: "IX_TeamLead_Id");
            RenameColumn(table: "dbo.Teams", name: "TeamLeadId", newName: "TeamLead_Id");
        }
    }
}
