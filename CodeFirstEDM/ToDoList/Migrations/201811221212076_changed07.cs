namespace ToDoList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changed07 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.PerformerSmallTasks", newName: "PerformerSmallTask1");
            CreateTable(
                "dbo.PerformerSmallTasks",
                c => new
                    {
                        PerformerId = c.Int(nullable: false),
                        SmallTaskId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PerformerId, t.SmallTaskId })
                .ForeignKey("dbo.Performers", t => t.PerformerId, cascadeDelete: true)
                .ForeignKey("dbo.SmallTasks", t => t.SmallTaskId, cascadeDelete: true)
                .Index(t => t.PerformerId)
                .Index(t => t.SmallTaskId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PerformerSmallTasks", "SmallTaskId", "dbo.SmallTasks");
            DropForeignKey("dbo.PerformerSmallTasks", "PerformerId", "dbo.Performers");
            DropIndex("dbo.PerformerSmallTasks", new[] { "SmallTaskId" });
            DropIndex("dbo.PerformerSmallTasks", new[] { "PerformerId" });
            DropTable("dbo.PerformerSmallTasks");
            RenameTable(name: "dbo.PerformerSmallTask1", newName: "PerformerSmallTasks");
        }
    }
}
