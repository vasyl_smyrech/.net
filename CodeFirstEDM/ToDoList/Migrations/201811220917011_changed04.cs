namespace ToDoList.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changed04 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Teams", "Performer_Id", "dbo.Performers");
            DropIndex("dbo.Teams", new[] { "Performer_Id" });
            DropColumn("dbo.Teams", "Performer_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Teams", "Performer_Id", c => c.Int());
            CreateIndex("dbo.Teams", "Performer_Id");
            AddForeignKey("dbo.Teams", "Performer_Id", "dbo.Performers", "Id");
        }
    }
}
