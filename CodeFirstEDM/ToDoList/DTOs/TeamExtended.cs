﻿namespace ToDoList.DTOs
{
    public class TeamExtended
    {
        public int Id { get; set; }
        public string TimNickName { get; set; }
        public string TeamLeadLastName { get; set; }
        public string PerformerLastName { get; set; }
        public string MainTaskTitle { get; set; }
    }
}
