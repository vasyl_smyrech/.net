﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ToDoList.DAL.Interfaces.ModelInterfaces;
using ToDoList.DAL.Interfaces.Interfaces.ModelInterfaces;


namespace ListToDo.DAL.DTOs
{
    public class MainTask : BaseModelDtoRepo, IMainTask
    {
        [Required]
        [StringLength(255)]
        public string TaskTitle { get; set; }

        [StringLength(510)]
        public string TaskDescription { get; set; }
        [StringLength(12)]
        public string Type { get; set; }
        public DateTime? EstimateFinish { get; set; }
        public double? WorkHoursRequaired { get; set; }
        [StringLength(25)]
        public string Status { get; set; }
        public DateTime? Startedwork { get; set; }
        public int? DonePercentage { get; set; }
        public int? TeamId { get; set; }

        public List<SmallTask> SmallTasks { get; set; }
        [ForeignKey ("TeamId")]
        public Team Team { get; set; }
    }
   
}
