﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ToDoList.DAL.Interfaces.ModelInterfaces;


namespace ListToDo.DAL.DTOs
{
    public class Performer : BaseModelDtoRepo, IPerformer
    {
        [Required]
        [StringLength(20)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(25)]
        public string LastName { get; set; }
        [Required]
        [StringLength(6)]
        public String ProfesionalLevel { get; set; }

        public List<SmallTask> SmallTasks { get; set; }
    }
}
