﻿using System;
using System.Collections.Generic;
using ToDoList.DAL.Interfaces.Interfaces.ModelInterfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ToDoList.DAL.Interfaces.ModelInterfaces;

namespace ListToDo.DAL.DTOs
{
    public class SmallTask : BaseModelDtoRepo, ISmallTask
    {
        [Required]
        [StringLength(255)]
        public string TaskTitle { get; set; }

        [Required]
        [StringLength(510)]
        public string TaskDescription { get; set; }
        [StringLength(25)]
        public string Type { get; set; }
        public DateTime? EstimateFinish { get; set; }
        public double? WorkHoursRequaired { get; set; }
        [StringLength(25)]
        public string Status { get; set; }
        public DateTime? Startedwork { get; set; }
        public int? DonePercentage { get; set; }
        [StringLength(300)]
        public string Remark { get; set; }
        public int? MainTaskId { get; set; }


        [ForeignKey("MainTaskId")]
        public  MainTask MainTask { get; set; }
        public  List<Performer> Performers { get; set; }

    }
}
