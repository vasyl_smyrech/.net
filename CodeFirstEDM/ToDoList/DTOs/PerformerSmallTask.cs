﻿using ListToDo.DAL.DTOs;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ToDoList.DAL.Interfaces.Interfaces.ModelInterfaces;


namespace ToDoList.DTOs
{
    public class PerformerSmallTask : IPerformerSmallTask
    {
        public Performer Performer { get; set; }
        public SmallTask SmallTask { get; set; }

        [Key, Column(Order = 1)]
        public int PerformerId { get; set; }
        [Key, Column(Order = 2)]
        public int SmallTaskId { get; set; }

    }
}
