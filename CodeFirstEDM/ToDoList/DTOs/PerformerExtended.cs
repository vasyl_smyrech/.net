﻿namespace ToDoList.DTOs
{
    public class PerformerExtended
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfesionalLevel { get; set; }
        public string SmallTask { get; set; }
    }
}
