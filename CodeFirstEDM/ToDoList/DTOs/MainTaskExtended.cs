﻿using System;

namespace ToDoList.DTOs
{
    public class MainTaskExtended
    {
        public int Id { get; set; }
        public string TaskTitle { get; set; }
        public string TaskDescription { get; set; }
        public string Type { get; set; }
        public DateTime EstimateFinish { get; set; }
        public double WorkHoursRequaired { get; set; }
        public string Status { get; set; }
        public DateTime Startedwork { get; set; }
        public int DonePercentage { get; set; }
        public int TeamId { get; set;}
        public string SmallTaskTitle { get; set; }
        public string Performer { get; set; }
    }
}
