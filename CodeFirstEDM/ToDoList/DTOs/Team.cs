﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using ToDoList.DAL.Interfaces.ModelInterfaces;


namespace ListToDo.DAL.DTOs
{
    public class Team : BaseModelDtoRepo, ITeam
    {
        [Required]
        [StringLength(20)]
        public string TimNickName { get; set; }
        [Required]       
        public int TeamLeadId { get; set; }

        public List<Performer> Performers { get; set; }
        public List<MainTask> MainTasks { get; set; }

        [ForeignKey("TeamLeadId")]
        public Performer PerformerTeamlead{ get; set; }
    }
}
