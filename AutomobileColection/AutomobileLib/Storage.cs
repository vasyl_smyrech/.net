﻿using System;
using Vehicles.BL;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace AutomobileBl
{
    
    public interface ISerializeCase
    {
        void SerializeData(object obj, VehicleEventArgs e);
        Vehicle[] DeserializeData(object obj, VehicleEventArgs e);
    } 
    public class BinarySerialize : ISerializeCase
    {
        public void SerializeData(object obj, VehicleEventArgs e)
        {
            Stream stream = File.Open(e.Path, FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter ();
            bf.Serialize(stream, e.GetVehicleList);
            stream.Close();
        }
        public Vehicle[] DeserializeData(object obj, VehicleEventArgs e)
        {
            Stream stream = File.Open(e.Path, FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            Vehicle[] VL = (Vehicle[])bf.Deserialize(stream);
            stream.Close();
            return VL;
        }
    }
    public class XmlSerialize : ISerializeCase
    {
        public void SerializeData(object obj,VehicleEventArgs e)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Vehicle[]));
            TextWriter writer = new StreamWriter(e.Path);
            serializer.Serialize(writer, e.GetVehicleList);
            writer.Close();
        }
        public Vehicle[] DeserializeData(object obj, VehicleEventArgs e)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(Vehicle[]));
            TextReader reader = new StreamReader(e.Path);
            Vehicle[] VL = (Vehicle[])deserializer.Deserialize(reader);
            reader.Close();
            return VL;
        }
    }

}
