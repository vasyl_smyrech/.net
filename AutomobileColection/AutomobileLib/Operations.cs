﻿using System;
using Buses.BL;
using Lorryes.BL;
using Vehicles.BL;
using System.IO;


namespace AutomobileBl
{
    public class Operations
    {
        public Vehicle[] VehicleList = new Vehicle[16];
        public delegate void OutputEventHandler(object obj, VehicleEventArgs args);
        public delegate void SerializeEventHandler(object obj, VehicleEventArgs args);
        public delegate Vehicle[] DeserializeEventHandler(object obj, VehicleEventArgs args);
        public void StartInit(string path)
        {
            //check file excisting and that it's not null 
            FileInfo pathInfo = new FileInfo(path);
            if (pathInfo.Exists && pathInfo != null)
            {
                BeforeStartingWork(path);
            }
            else { AddExampleList(); }           
        }
        //if it's needed adds few vehicle examples
        public void AddExampleList()
        {
            VehicleList[0] = new Lorry(1, "KRAZ", "Lisnyk", 38.5, 1997, 17.3);
            VehicleList[1] = new Lorry(2, "Maz", "M2542", 36, 1999, 16.1);
            VehicleList[2] = new Lorry(3, "Renault", "Magnum", 39.5, 2010, 15.9);
            VehicleList[3] = new Lorry(4, "DAF", "F556", 40, 2013, 17);
            VehicleList[4] = new Bus(5, "Ikarus", "M5522", 30.5, 1997, 50);
            VehicleList[5] = new Bus(6, "LAZ", "Turist", 28.5, 2000, 48);
            VehicleList[6] = new Bus(7, "Neoplan", "Comfort", 27.8, 2010, 36);
        }
        public void ShowVehicles(string typeOfVehicle)
        {
            OnGotResult(VehicleList, typeOfVehicle);
        }
        public void ShowVehicleList()
        {
            OnGotResult(VehicleList);
        }
        public event OutputEventHandler GotResult;
        protected virtual void OnGotResult(Vehicle[] vehicleList)
        {
            GotResult?.Invoke(this, new VehicleEventArgs() { GetVehicleList = vehicleList });
        }
        protected virtual void OnGotResult(Vehicle[] vehicleList, string typeOfVhicle)
        {
            GotResult?.Invoke(this, new VehicleEventArgs() { GetVehicleList = vehicleList, TypeOfVehicle = typeOfVhicle });
        }
        public void SortByTrademark()
        {
            Array.Sort(VehicleList, new VehicleCompareTrademark());
        }
        public void SortByFirstRegistration()
        {
            Array.Sort(VehicleList);
        }
        public void SortByCommonWeight()
        {
            Array.Sort(VehicleList, new VehicleCompareCommonWeight());
        }
        public void SortBySequenceNumber()
        {
            Array.Sort(VehicleList, new VehicleCompareSequenceNumber());
        }
        public void DeleteVehicle(int sequenceNumber)
        {
            this.VehicleList[sequenceNumber] = null;
        }
        public void AddVehicleTrademark(int index, string mark)
        {
            VehicleList[index].TradeMark = mark;
        }
        public void AddVehicleModel(int index, string model)
        {
            VehicleList[index].Model = model;
        }
        public void AddVehicleCommonWeight(int index, double weight)
        {
            VehicleList[index].CommonWeight = weight;
        }
        public void AddVehicleFirstRegistration(int index, int year)
        {
            VehicleList[index].FirstRegistration = year;
        }
        public void VehicleListXmlSerialization(string path)
        {
            AfterListChangedXml(VehicleList, path);
        }
        public void VehicleListBinSerialization(string path)
        {
            AfterListChangedBin(VehicleList, path);
        }
        public void XmlSerialization()
        {
            OnGotResult(VehicleList);
        }
        public event DeserializeEventHandler StartWorkingWithList;
        protected virtual void BeforeStartingWork(string path)
        {
            if (StartWorkingWithList != null) VehicleList = StartWorkingWithList(this, new VehicleEventArgs {Path = path});
           
        }
        public event SerializeEventHandler ListChangedBin;
        public event SerializeEventHandler ListChangedXml;
        protected virtual void AfterListChangedBin(Vehicle[] vehicleList, string path)
        {
            ListChangedBin?.Invoke(this, new VehicleEventArgs() { GetVehicleList = vehicleList, Path = path });
        }
        protected virtual void AfterListChangedXml(Vehicle[] vehicleList, string path)
        {
            ListChangedXml?.Invoke(this, new VehicleEventArgs() { GetVehicleList = vehicleList, Path = path });
        }

    }
    public class VehicleEventArgs : EventArgs
    {
        public Vehicle[] GetVehicleList { get; set; }
        public string TypeOfVehicle { get; set; }
        public  string Path { get; set; }
    }
}

