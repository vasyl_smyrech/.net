﻿using System;
using System.Xml.Serialization;
using Buses.BL;
using Lorryes.BL;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Vehicles.BL
{
    [Serializable()]
    [XmlInclude(typeof(Bus))]
    [XmlInclude(typeof(Lorry))]
    public abstract class Vehicle : IComparable
    {
        private const int allowedExpluatationPeriod = 20;// years
        private int sequenceNumber;
        private string tradeMark;
        private string model;
        private double commonWeight;
        private int firstRegistration;//year yyyy
        public Vehicle()
        { }
        public Vehicle(int sequenceNumber, string tradeMark, string model, double commonWeight, int firstRegistration)
        {
            this.tradeMark = tradeMark;
            this.model = model;
            this.commonWeight = commonWeight;
            this.firstRegistration = firstRegistration;
            this.sequenceNumber = sequenceNumber;
        }
        public int SequenceNumber
        {
            get { return sequenceNumber; }
            set { sequenceNumber = value; }

        }

        public string TradeMark
        {
            get { return tradeMark; }
            set { tradeMark = value; }
        }
        public string Model
        {
            get { return model; }
            set { model = value; }
        }
        public double CommonWeight
        {
            get { return commonWeight; }
            set { commonWeight = value; }
        }
        public int FirstRegistration
        {
            get { return firstRegistration; }
            set { firstRegistration = value; }
        }
        public abstract double CarryAbility();

        public int CompareTo(object obj)
        {
#pragma warning disable IDE0019 // Use pattern matching
            var v = obj as Vehicle;
#pragma warning restore IDE0019 // Use pattern matching
            if (v != null)
            {
                if (this.firstRegistration < v.firstRegistration)
                    return -1;
                else if (this.firstRegistration > v.firstRegistration)
                    return 1;
                else return 0;
            }
            else { throw new Exception("Parameter must be of type exception!"); }
        }

        public int RemainExpluatation(int firstRegistration)
        {
            return DateTime.Today.Year - firstRegistration;
        }

    }
    public class VehicleCompareSequenceNumber : IComparer<Vehicle>
    {
        int IComparer<Vehicle>.Compare(Vehicle x, Vehicle y)
        {
            if ((x != null) && (y != null))
            {
                if (x.SequenceNumber > y.SequenceNumber) return 1;
                else if (x.SequenceNumber < y.SequenceNumber) return -1;
                else return 0;
            }
            else return 0;
        }
    }
    public class VehicleCompareCommonWeight : IComparer<Vehicle>
    {
        int IComparer<Vehicle>.Compare(Vehicle x, Vehicle y)
        {
            if ((x != null) && (y != null))
            {
                if (x.CommonWeight > y.CommonWeight) return 1;
                else if (x.CommonWeight < y.CommonWeight) return -1;
                else return 0;
            }
            else return 0;
        }
    }
    public class VehicleCompareTrademark : IComparer<Vehicle>
    {
        int IComparer<Vehicle>.Compare(Vehicle x, Vehicle y)
        {
            if ((x != null) && (y != null))
            {
                if (x.TradeMark[0] > y.TradeMark[0]) return 1;
                else if (x.TradeMark[0] < y.TradeMark[0]) return -1;
                else return 0;
            }
            else return 0;
        }
    }


}
