﻿using Vehicles.BL;
using System;
using System.Runtime.Serialization;


namespace Lorryes.BL
{
    [Serializable()]
    public class Lorry : Vehicle, ISerializable
    {
        private double emptyWeight;     
        public double EmptyWeight
        {
            get { return emptyWeight; }
            set { emptyWeight = value; }
        }
        public Lorry() { }
        public Lorry(int sequenceNumber, string tradeMark, string model, double commonWeight, int firstRegistration, double emptyWeight) : base(sequenceNumber, tradeMark, model, commonWeight, firstRegistration)
        {
            this.emptyWeight = emptyWeight;
        }
        public override double CarryAbility()
        {
            return CommonWeight - EmptyWeight;            
        }
        //Constructors for binary serialization/deserialization
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("SequenceNumber", SequenceNumber);
            info.AddValue("TradeMark", TradeMark);
            info.AddValue("Model", Model);
            info.AddValue("FirstRegistration", FirstRegistration);
            info.AddValue("CommonWeight", CommonWeight);
            info.AddValue("EmptyWeight", EmptyWeight);

        }

        public Lorry(SerializationInfo info, StreamingContext context)
        {
            SequenceNumber = (int)info.GetValue("SequenceNumber", typeof(int));
            TradeMark = (string)info.GetValue("TradeMark", typeof(string));
            Model = (string)info.GetValue("Model", typeof(string));
            FirstRegistration = (int)info.GetValue("FirstRegistration", typeof(int));
            CommonWeight = (double)info.GetValue("CommonWeight", typeof(double));
            EmptyWeight = (double)info.GetValue("EmptyWeight", typeof(double));
        }

    }
}