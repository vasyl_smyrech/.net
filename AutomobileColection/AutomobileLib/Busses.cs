﻿using Vehicles.BL;
using System;
using System.Runtime.Serialization;


namespace Buses.BL
{
    [Serializable()] 
    public class Bus : Vehicle, ISerializable
    {  
            private int totalNumberOfSeats;
            public int TotalNumberOfSeats
            {
                get { return totalNumberOfSeats; }
                set { totalNumberOfSeats = value; }
            }
            public Bus() { }
            public Bus(int sequenceNumber, string tradeMark, string model, double commonWeight, int firstRegistration, int totalNumberOfSeats) : base(sequenceNumber, tradeMark, model, commonWeight, firstRegistration)
            {
                this.totalNumberOfSeats = totalNumberOfSeats;
            }
            public override double CarryAbility()
            {
                return Convert.ToDouble(totalNumberOfSeats - 1); //Total seats number - driver seat 
            }
            //Constructors for binary serialization/deserialization
            public void GetObjectData(SerializationInfo info, StreamingContext context)
            {
                info.AddValue("SequenceNumber", SequenceNumber);
                info.AddValue("TradeMark", TradeMark);
                info.AddValue("Model", Model);
                info.AddValue("FirstRegistration", FirstRegistration);
                info.AddValue("CommonWeight", CommonWeight);
                info.AddValue("TotalNumberOfSeats", TotalNumberOfSeats);

            }

            public Bus(SerializationInfo info, StreamingContext context)
            {
                SequenceNumber = (int)info.GetValue("SequenceNumber", typeof(int));
                TradeMark = (string)info.GetValue("TradeMark", typeof(string));
                Model = (string)info.GetValue("Model", typeof(string));
                FirstRegistration = (int)info.GetValue("FirstRegistration", typeof(int));
                CommonWeight = (double)info.GetValue("CommonWeight", typeof(double));
                TotalNumberOfSeats = (int)info.GetValue("TotalNumberOfSeats", typeof(int));
            }


    }
}
