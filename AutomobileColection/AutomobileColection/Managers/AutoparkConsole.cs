﻿using System;
using Buses.BL;
using Lorryes.BL;
using AutomobileBl;
using System.Collections.Generic;

namespace VehicleColection.Managers
{

    public class AutoparkConsole
    {
        //paths for serialisation
        private string xmlPath = "VehiclesData.xml";
        private string datPath = "VehiclesData.Dat";
        public string XmlPath
        {
            get { return xmlPath; }
            set { xmlPath = value; }
        }
        public string DatPath
        {
            get { return datPath; }
            set { datPath = value; }
        }
        //The List below uses for finding first absent number in the array
        List<int> AllSeqNumInArray = new List<int>();
        Operations ListOper = new Operations();
        private BinarySerialize BS = new BinarySerialize();
        private XmlSerialize XS = new XmlSerialize();
        //Input registration date enter a user
        private int firstRegistrationInput;
        //Output is checked by CheckFirstRegistrationInput() method
        private int firstRegistrationOutput;
        //sequenceNumberOutput is 
        private int sequenceNumberOutput;
        //sequenceNumberAnswer - it's a vehicle number user enter to modify data
        private int sequenceNumberAnswer;
        //the numberInArray is used in case corecting vehicle
        private int numberInArray = 1000;
        //the common weight entered by the user
        private double commonWeight;
        //the common weight checked by CheckCommonWeight() method
        private double commonWeightOutput;
        //seat number defined in Ability(string vehicleType) method
        private int seatsNumberOutput;
        //load-in ability defined in Ability(string vehicleType) method
        private double loadInAbilityOutput;
        //trademarkOutput is checked user input data by CheckTrademark() method
        private string trademarkOutput;
        //modelOutput is checked user input data by CheckModel() method
        private string modelOutput;
        public void StartInitialisation()
        {
            ListOper.StartWorkingWithList += XS.DeserializeData;
            ListOper.ListChangedBin += BS.SerializeData;
            ListOper.ListChangedXml += XS.SerializeData;
            ListOper.StartInit(XmlPath);    
            int countObj = 0;           
            //if there is no one vehicle in the VehicleList adds instances
            for (int i = 0; i < ListOper.VehicleList.Length; i++)
            {
                if (ListOper.VehicleList[i] == null) countObj++;
            }
            if (countObj == ListOper.VehicleList.Length)
            {
                ListOper.AddExampleList();
            }
            ListOper.GotResult += new OutputInformationConsole().OutputTable;
        }
        public void Greeting()
        {
            Console.WriteLine("\n\t\t\t   Hello. You are in LTD 'AUTOPARK' information program.");
            FirstQuestion();
        }
        private void FirstQuestion()
        {
            Console.Clear();
            Console.WriteLine("\n\t\t\t     Enter appropriate item number, then press 'Enter':");
            InnerFirstQuestion();
        }
        private void InnerFirstQuestion()
        {
            Console.Clear();
            Console.WriteLine("\n\t1 - Operations with Lorries;\t 2 - Operations with Buses;\t 3 - Common list;    4 - Exit.");
            Console.Write("\t");
            string answerOne = Console.ReadLine();
            switch (answerOne)
            {
                case "1":
                    VehiclesMenu("lorry");
                    break;
                case "2":
                    VehiclesMenu("bus");
                        break;
                case "3":
                    ListMenu();
                    break;
                case "4":
                    {
                        //ListOper.VehicleListSerealization(XmlPath);
                        //ListOper.VehicleListSerealization(DatPath);
                    }
                    break;
                default:
                    Console.WriteLine("\n\tYou entered wrong key button. Type 1, 2, 3 or 4 on reyboard, then press 'Enter':");
                    InnerFirstQuestion();
                    break;
            }
        }
        private void ListMenu()
        {
            Console.Clear();
            Console.WriteLine("\n\t\t\tEnter apropriate item number, then press 'Enter':" +
            "\n\t1 - Sort by sequence number;\t\t3 - Sort by trademark;\t\t5 - Back;" +
            "\n\t2 - Sort by first registrayion;\t\t4 - Sort by common weight;\t6 - Exit.");
            Console.Write("\t");
            string answerCommonList = Console.ReadLine();
            if (answerCommonList == "1")
            {
                ListOper.SortBySequenceNumber();
                Console.WriteLine("\n\tFull List of buses and lorries sorted by sequence number:");
                ListOper.ShowVehicleList();
                Console.WriteLine("\n\tYou will be returned to previous Menu. Press 'Enter'.");
                Console.Write("\t");
                Console.ReadKey();
                ListMenu();
            }
            else if (answerCommonList == "2")
            {
                ListOper.SortByFirstRegistration();
                Console.WriteLine("\n\tFull List of buses and lorries sorted by year of first registration:\n");
                ListOper.ShowVehicleList();
                Console.WriteLine("\n\tYou will be returned to previous Menu. Press 'Enter'.\n");
                Console.Write("\t");
                Console.ReadKey();
                ListMenu();
            }
            else if (answerCommonList == "3")
            {
                ListOper.SortByTrademark();
                Console.WriteLine("\n\tFull List of buses and lorries sorted by trademark:\n");
                ListOper.ShowVehicleList();
                Console.WriteLine("\n\tYou will be returned to previous Menu. Press 'Enter'.\n");
                Console.Write("\t");
                Console.ReadKey();
                ListMenu();
            }
            else if (answerCommonList == "4")
            {
                ListOper.SortByCommonWeight();
                Console.WriteLine("n\tFull List of buses and lorries sorted by common weight:\n");
                ListOper.ShowVehicleList();
                Console.WriteLine("\n\tPress 'Enter'.You will be returned to previous Menu. Press 'Enter'.\n");
                Console.Write("\t");
                Console.ReadKey();
                ListMenu();
            }
            else if (answerCommonList == "5")
            {
                FirstQuestion();
            }
            else if (answerCommonList == "6")
            {
                //ListOper.VehicleListSerealization(XmlPath);
                //ListOper.VehicleListSerealization(DatPath);
            }
            else
            {
                ListMenu();
            }
        }
        private void VehiclesMenu(string vehicleType)
        {
            Console.Clear();
            Console.WriteLine("\n\t\t\t     Enter appropriate item number, then press 'Enter':");
            InnerVehiclesMenu(vehicleType);
        }
        private void InnerVehiclesMenu(string vehicle)
        {
            Console.Clear();
            Console.WriteLine("\n\t1 - Add a {0};     3 - Correct a {0};   5 - Sort by date of first registration; 7 - Exit." +
            "\n\t2 - Remove a {0};  4 - Sort by trademark; 6 - Back;", vehicle);
            Console.Write("\t");
            string answerLorryOrBuss = Console.ReadLine();
            switch (answerLorryOrBuss)
            {
                case "1":
                    AddVehicle(vehicle);
                    break;
                case "2":
                    RemoveVehicle(vehicle);
                    break;
                case "3":
                    CorrectVehicle(vehicle);
                    break;
                case "4":
                    ListOper.SortByTrademark();
                    ListOper.ShowVehicles(vehicle);
                    Console.WriteLine("\n\tYou will be returned to previous Menu. Press 'Enter'.");
                    Console.Write("\t");
                    Console.ReadKey();
                    InnerVehiclesMenu(vehicle);
                    break;
                case "5":
                    ListOper.SortByFirstRegistration();
                    ListOper.ShowVehicles(vehicle);
                    Console.WriteLine("\n\tYou will be returned to previous Menu. Press 'Enter'.");
                    Console.Write("\t");
                    Console.ReadKey();
                    InnerVehiclesMenu(vehicle);
                    break;
                case "6":
                    FirstQuestion();
                    break;
                case "7":
                    {
                        //ListOper.VehicleListSerealization(XmlPath);
                        //ListOper.VehicleListSerealization(DatPath);
                    }
                    break;
                default:
                    Console.WriteLine("\n\tYou entered wrong key button. Type 1, 2, 4, 5, 6, or 7 on keyboard, then press 'Enter':");
                    InnerVehiclesMenu(vehicle);
                    break;
            }
        }
        //adds new vehicle to the VehicleList
        private void AddVehicle(string vehicleType)
        {
            Console.Clear();
            sequenceNumberOutput = 0;
            //return first index in the array where object = null
            int emptySpaceInVehicleList = Array.IndexOf(ListOper.VehicleList, null);
            //writes all sequence numbers from objects in ListOper.VehicleList
            CheckSequenceNumber();
            //sorts all sequence numbers in ascending order 
            AllSeqNumInArray.Sort();
            //cheks first free sequence number
            for (int i = 1; i <= ListOper.VehicleList.Length; i++)
            {
                if (sequenceNumberOutput == 0)
                    if (! AllSeqNumInArray.Contains(i))
                    {
                        sequenceNumberOutput = i;
                    }
            }
            Console.WriteLine("\n\tSequence number of the {1} you are adding is {0}.", sequenceNumberOutput, vehicleType);
            //input trademark
            Console.Write("\n\tEnter a trademark of the {0} then press 'Enter':  ", vehicleType);
            CheckTrademark();
            //input model
            Console.Write("\n\tEnter a model of the {0} then press 'Enter':  ", vehicleType);
            CheckModel();
            //input first registration
            Console.Write("\n\tEnter a year of the {0} first registration (1950 - {1}) then press 'Enter':  ", vehicleType, DateTime.Today.Year);
            CheckFirstRegistrationInput();
            //input common weight
            Console.Write("\n\tEnter common weight (tons) of the {0} (0 - 100) then press 'Enter':  ", vehicleType);
            CheckCommonWeight();           
            //input load-in ability for lorries or number of seats for buses
            Ability(vehicleType);
            if (vehicleType == "lorry") { ListOper.VehicleList[emptySpaceInVehicleList] = new Lorry(sequenceNumberOutput, trademarkOutput, modelOutput, commonWeightOutput, firstRegistrationOutput, loadInAbilityOutput); }
            else if (vehicleType == "bus") { ListOper.VehicleList[emptySpaceInVehicleList] = new Bus(sequenceNumberOutput, trademarkOutput, modelOutput, commonWeightOutput, firstRegistrationOutput, seatsNumberOutput); }
            Console.WriteLine("\n\tYour {0} added. You will be returned to previous Menu.", vehicleType);
            //Serialization
            ListOper.VehicleListXmlSerialization(XmlPath);
            ListOper.VehicleListBinSerialization(DatPath);
            VehiclesMenu(vehicleType);
        }
        private void CorrectVehicle(string vehicleType)
        {
            Console.Clear();
            Console.WriteLine("\n\tEnter sequence number of the {0} you want modify then press 'Enter'.  ", vehicleType);
            ListOper.ShowVehicles(vehicleType);
            if (!CheckSequenceNumber(vehicleType)) CorrectVehicle(vehicleType); 
            Console.WriteLine("\n\tChose data you want change in the {0} then press 'Enter'.\n\t1 - Change the trademark;\t3 - Change the year of first registration;\t5 - Back." +
                "\n\t2 - Change the model;\t\t4 - Change the common weight;", vehicleType);
            Console.Write("\t");
            string correctAnswer = Console.ReadLine();
            switch (correctAnswer)
            {
                case "1":
                    Console.Write("\n\tEnter a trademark of the {0} then press 'Enter':  ", vehicleType);
                    CheckTrademark();
                    ListOper.AddVehicleTrademark(numberInArray, trademarkOutput);
                    break;
                case "2":
                    Console.Write("\n\tEnter a model of the {0} then press 'Enter':  ", vehicleType);
                    CheckModel();
                    ListOper.AddVehicleModel(numberInArray, modelOutput);
                    break;
                case "3":
                    Console.Write("\n\tEnter a year of the {0} first registration (1950 - {1}) then press 'Enter':  ", vehicleType, DateTime.Today.Year);
                    CheckFirstRegistrationInput();
                    ListOper.AddVehicleFirstRegistration(numberInArray, firstRegistrationOutput);
                    break;
                case "4":
                    Console.Write("\n\tEnter common weight (tons) of the {0} (0 - 100) then press 'Enter':  ", vehicleType);
                    CheckCommonWeight();
                    ListOper.AddVehicleCommonWeight(numberInArray, commonWeightOutput);
                    break;
                case "5":
                    VehiclesMenu(vehicleType);
                    break;
                default:
                    Console.WriteLine("\n\tYou entered invalid data. Enter 1, 2, 3, 4 or 5 then press 'Enter'");
                    CorrectVehicle(vehicleType);
                    break;
            }
            Console.WriteLine("\n\tYour {0} modified. You will be returned to previous Menu.", vehicleType);
            ListOper.VehicleListXmlSerialization(XmlPath);
            ListOper.VehicleListBinSerialization(DatPath);
            VehiclesMenu(vehicleType);
        }
        private void RemoveVehicle(string vehicleType)
        {
            Console.Clear();
            //Defines whether is or not atleast one object of type Lorry or Bus. 
            //If isn't shows approiate text then returns to previous menu.
            int lorryCount = 0;
            int bussCount = 0;
            foreach (var vehicle in ListOper.VehicleList)
                if (vehicle is Lorry) { lorryCount++; }
                else if (vehicle is Bus) { bussCount++; }
            if (((vehicleType == "lorry") && (lorryCount == 0)) || ((vehicleType == "bus") && (bussCount == 0)))
            {
                Console.WriteLine(" \n\tYour Vehicles list doesn't contain no one {0}. \n\tYou will be reterned to previous Menu. Press any button.", vehicleType);
                VehiclesMenu(vehicleType);
            }
            //Proposes chose sequence number of vehicle then remove chosen number and returns to previous menu.
            Console.WriteLine("\n\tEnter number of {0} you want remove from the following list, then press 'Enter':", vehicleType);
            ListOper.ShowVehicles(vehicleType);
            if (CheckSequenceNumber(vehicleType))
            {
                {
                    ListOper.DeleteVehicle(numberInArray);
                    Console.WriteLine("\n\tChoosen {0} is deleted.", vehicleType);
                    ListOper.VehicleListXmlSerialization(XmlPath);
                    ListOper.VehicleListBinSerialization(DatPath);
                    Console.WriteLine("\tYou will be returned to previous Menu. Press any button.");
                    Console.Write("\t");
                    Console.ReadKey();
                    VehiclesMenu(vehicleType);
                }
            }
            else RemoveVehicle(vehicleType);
        }
        //Capitalise first letter end cuts all letters after the 13th one
        private void CheckTrademark()
        {
            Console.Clear();
            Console.Write("\t");
            string trademarkInput = Convert.ToString(Console.ReadLine());
            //the first character in the trademark into a capital letter
            try
            {
                string capitalLetter = trademarkInput[0].ToString();
                capitalLetter = capitalLetter.ToUpper();
                trademarkInput = trademarkInput.Remove(0, 1);
                trademarkOutput = trademarkInput.Insert(0, capitalLetter);
            }
            catch (Exception)
            {
                Console.WriteLine("\tTrademark can't be empty! Try again:");
                CheckTrademark();
            }
            //cuts all letters after 13
            if (trademarkOutput.Length > 14)
            trademarkOutput = trademarkInput.Remove(14);
        }
        //Capitalise first letter end cuts all letters after the 11th one
        private void CheckModel()
        {

            Console.Write("\t");
            string modelInput = Convert.ToString(Console.ReadLine());
            //the first character in the trademark into a capital letter
            try
            {
                string capitalLetter = modelInput[0].ToString();
                capitalLetter = capitalLetter.ToUpper();
                modelInput = modelInput.Remove(0, 1);
                modelOutput = modelInput.Insert(0, capitalLetter);
            }catch (Exception)
            {
                Console.WriteLine("Model can't be empty! Try again:");
                CheckModel();
            }
            //cuts all letters after 11
            if (modelOutput.Length > 12)
            modelOutput = modelInput.Remove(12);
        }
        //adds all excisting sequence numbers used in ListOper.VehicleList objects 
        private void CheckSequenceNumber()
        {           
            for (int i = 0; i < ListOper.VehicleList.Length; i++)
            {
                try { 
                    for (int j = 1; j <= ListOper.VehicleList.Length; j++)
                    {
                        if (ListOper.VehicleList[i].SequenceNumber == j)
                        {
                        AllSeqNumInArray.Add(j);
                        }
                    }
                }
                catch (Exception) { }
            }          
        }
        //checks existence vehicle with appropriate vehicle type of entered data by user  
        private bool CheckSequenceNumber(string vehicle)
        {
            try
            {
                Console.Write("\t");
                sequenceNumberAnswer = Int32.Parse(Console.ReadLine());
                if (sequenceNumberAnswer <= ListOper.VehicleList.Length)
                {
                    for (int i = 0; i < ListOper.VehicleList.Length; i++)
                    {
                        try
                        {
                            if (ListOper.VehicleList[i].SequenceNumber == sequenceNumberAnswer)
                            {

                                if (((ListOper.VehicleList[i] is Lorry) && (vehicle == "bus")) || ((ListOper.VehicleList[i] is Bus) && (vehicle == "lorry")))
                                {
                                    Console.WriteLine("\n\tThe vehicle with sequence number {0} is not a {1}.\n\t" +
                                    "You will be returned to previous Menu. Press 'Enter'", sequenceNumberAnswer, vehicle);
                                    Console.Write("\t");
                                    Console.ReadKey();
                                }
                                else if (((ListOper.VehicleList[i] is Lorry) && (vehicle == "lorry")) || ((ListOper.VehicleList[i] is Bus) && (vehicle == "bus")))
                                {
                                    numberInArray = i;
                                    return true;
                                }
                            }
                        }
                        catch (Exception) { }
                    }
                    if (numberInArray == 1000)
                    {
                        Console.WriteLine("\n\tThere isn't a vehicle with sequence number {0} in the vehicle list.\n" +
                        "\tYou will be returned to previous Menu. Press 'Enter'", sequenceNumberAnswer);
                        Console.Write("\t");
                        Console.ReadKey();
                        return false;
                    }
                }
                else
                {
                    Console.WriteLine("\n\tThere isn't a vehicle with sequence number {0} in the vehicle list.\n" +
                                 "\tYou will be returned to previous Menu. Press 'Enter'", sequenceNumberAnswer);
                    Console.Write("\t");
                    Console.ReadKey();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("\n\tYou entered invalid data. You will be returned to previous Menu. Press 'Enter'");
                Console.ReadKey();
                return false;
            }          
            return false;
        }
        //checks accuracy common weight data
        private void CheckCommonWeight()
        {
            try
            {
                commonWeight = double.Parse(Console.ReadLine());
                if ((commonWeight < 0) || (commonWeight > 100))
                {
                    Console.Write("\n\tYou entered the weight out of the range 0 - 100 tons. Try again:  ");
                    CheckCommonWeight();
                }
            }
            catch (Exception)
            {
                Console.Write("\n\tYou entered invalid data. The weight must be integer or decimal of range 0 - 100 tons. Try again:  ");
                CheckCommonWeight();
            }
            commonWeightOutput = commonWeight;
        }
        //checks data acuracy for buses - number of seats, for lories - load-in ability
        private void Ability(string vehicleType)
        {
            double loadInAbility;
            int seatsNumber;
            try
            {
                if (vehicleType == "lorry")
                {
                    Console.Write("\n\tEnter empty weight (ton) of the {0} (0 - 85) then press 'Enter':  ", vehicleType);
                    Console.Write("\n\t");
                    loadInAbility = double.Parse(Console.ReadLine());
                    if ((loadInAbility < 0) || (loadInAbility > 85))
                    {
                        Console.WriteLine("\n\tYou entered the empty weight out of the range 0 - 85 tons.");
                        Ability(vehicleType);
                    }
                    loadInAbilityOutput = loadInAbility;
                }
                else if (vehicleType == "bus")
                {
                    Console.Write("\n\tEnter common seats  number of the {0} (1 - 120) then press 'Enter':  ", vehicleType);
                    Console.Write("\t");
                    seatsNumber = Int32.Parse(Console.ReadLine());
                    if ((seatsNumber < 1) || (seatsNumber > 120))
                    {
                        Console.WriteLine("\n\tYou entered the seats number out of the range 0 - 120 seats.");
                        Ability(vehicleType);
                    }
                    seatsNumberOutput = seatsNumber;
                }
            }
            catch (Exception)
            {
                Console.Write("\nYou entered invalid data.");
                Ability(vehicleType);
            }
        }
        //checks accuracy first registration data
        private void CheckFirstRegistrationInput()
        {
            try
            {
                Console.Write("\t");
                firstRegistrationInput = Int32.Parse(Console.ReadLine());
                if ((firstRegistrationInput < 1950) || (firstRegistrationInput > DateTime.Today.Year))
                {
                    Console.Write("\n\tYou entered the year out of the range 1950 - {0}. Try again:  ", DateTime.Today.Year);
                    CheckFirstRegistrationInput();
                }
            }
            catch (Exception)
            {
                Console.Write("\n\tYou entered invalid data. The year must be of range 1950 - {0}. Try again:  ", DateTime.Today.Year);
                CheckFirstRegistrationInput();
            }
            firstRegistrationOutput = firstRegistrationInput;
        }       
    }
}
