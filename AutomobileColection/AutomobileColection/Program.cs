﻿using VehicleColection.Managers;


namespace Program.App
{
    class Program
    {
        static void Main(string[] args)
        {          
            AutoparkConsole AConsole = new AutoparkConsole();
            AConsole.StartInitialisation();
            AConsole.Greeting();
        }
    }
}
