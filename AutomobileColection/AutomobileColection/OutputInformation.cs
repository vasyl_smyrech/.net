﻿using System;
using AutomobileBl;
using Buses.BL;
using Lorryes.BL;
using Vehicles.BL;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VehicleColection
{
    public interface IOutputInformation
    {
        void OutputTable(object obj, VehicleEventArgs e);
        void OutputMessage();
    }
    class OutputInformationConsole : IOutputInformation
    {
        public void OutputTable(object obj, VehicleEventArgs e)
        {
            if (e.TypeOfVehicle is null)
            {
                Console.WriteLine("     ---------------------------------------------------------------------------------------------------\n" +
                                  "    | SEQUENCE |  TYPE   |  MANUFACTURER  |     MODEL    | YEAR OF FIRST |     TOTAL    |    LOAD-IN    |\n" +
                                  "    |  NUMBER  |         |                |              | REGISTRATION  | WEIGHT, TONS |    ABILITY    |\n" +
                                  "    |---------------------------------------------------------------------------------------------------|");
                try
                {
                    for (int i = 0; i < e.GetVehicleList.Length; i++)
                        if (e.GetVehicleList[i] is Bus)
                        {
                            string typeOfVehicle = "Bus";
                            Console.WriteLine("    |    {0, -2}    |  {1, -5}  |  {2, -14}|  {3, -12}|     {4}      |     {5, -7}  |   {6, -5}seats  |", e.GetVehicleList[i].SequenceNumber, typeOfVehicle,
                            e.GetVehicleList[i].TradeMark, e.GetVehicleList[i].Model, e.GetVehicleList[i].FirstRegistration, e.GetVehicleList[i].CommonWeight, e.GetVehicleList[i].CarryAbility());
                        }
                        else if (e.GetVehicleList[i] is Lorry)
                        {
                            string typeOfVehicle = "Lorry";
                            Console.WriteLine("    |    {0, -2}    |  {1, -5}  |  {2, -14}|  {3, -12}|     {4}      |     {5, -7}  |   {6, -5}tons   |", e.GetVehicleList[i].SequenceNumber, typeOfVehicle,
                        e.GetVehicleList[i].TradeMark, e.GetVehicleList[i].Model, e.GetVehicleList[i].FirstRegistration, e.GetVehicleList[i].CommonWeight, e.GetVehicleList[i].CarryAbility());
                        }
                }
                catch (Exception) { }
                Console.WriteLine("     ---------------------------------------------------------------------------------------------------\n");
            }
            else
            {
                Console.WriteLine("     ---------------------------------------------------------------------------------------------------\n" +
                                  "    | SEQUENCE |  TYPE   |  MANUFACTURER  |     MODEL    | YEAR OF FIRST |     TOTAL    |    LOAD-IN    |\n" +
                                  "    |  NUMBER  |         |                |              | REGISTRATION  | WEIGHT, TONS |    ABILITY    |\n" +
                                  "     ---------------------------------------------------------------------------------------------------");
                try
                {
                    for (int i = 0; i < e.GetVehicleList.Length; i++)
                    {
                        if (e.TypeOfVehicle == "lorry")
                        {
                            if (e.GetVehicleList[i] is Lorry)
                            {
                                Console.WriteLine("    |    {0, -2}    |  {1, -5}  |  {2, -14}|  {3, -12}|     {4}      |     {5, -7}  |   {6, -5}tons   |", e.GetVehicleList[i].SequenceNumber, e.TypeOfVehicle,
                          e.GetVehicleList[i].TradeMark, e.GetVehicleList[i].Model, e.GetVehicleList[i].FirstRegistration, e.GetVehicleList[i].CommonWeight, e.GetVehicleList[i].CarryAbility());
                            }
                        }
                        if (e.TypeOfVehicle == "bus")
                        {
                            if (e.GetVehicleList[i] is Bus)
                            {
                                Console.WriteLine("    |    {0, -2}    |  {1, -5}  |  {2, -14}|  {3, -12}|     {4}      |     {5, -7}  |   {6, -5}seats  |", e.GetVehicleList[i].SequenceNumber, e.TypeOfVehicle,
                        e.GetVehicleList[i].TradeMark, e.GetVehicleList[i].Model, e.GetVehicleList[i].FirstRegistration, e.GetVehicleList[i].CommonWeight, e.GetVehicleList[i].CarryAbility());
                            }
                        }
                    }

                }
                catch (Exception) { }
                Console.WriteLine("     ---------------------------------------------------------------------------------------------------\n");
            }
        }
        public void OutputMessage()
        {

        }
    }
}
